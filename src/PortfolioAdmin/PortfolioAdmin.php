<?php
/**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 8/28/2018
 * Time: 9:45 PM
 */

namespace App\PortfolioAdmin;


use App\Message\Message;
use App\Model\Database;
use PDO;

class PortfolioAdmin extends Database
{


    public $id;
    public $project_type;
    public $project_title;
    public $link;
    public $image;


    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("project_type", $postArray)) {
            $this->project_type = $postArray['project_type'];
        }

        if (array_key_exists("project_title", $postArray)) {
            $this->project_title = $postArray['project_title'];
        }

        if (array_key_exists("link", $postArray)) {
            $this->link = $postArray['link'];
        }

        if (array_key_exists("image", $postArray)) {
            $this->image = $postArray['image'];
        }


    }// end of setData()


    public function store()
    {
        $dataArray = [$this->project_type, $this->project_title, $this->link, $this->image];
        $sqlStr = "INSERT INTO `portfolio` (`project_type`,`project_title`,`link`,`image`) VALUES (?,?,?,?)";
        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Success! Inserted Data.....");
        } else {

            Message::message("Error!! Data Does Not Inserted....");
        }

    }

    // end of store()
    public function index()
    {

        $sqlQuery = "select * from `portfolio`";
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    public function view($id)
    {

        $sqlQuery = "select * from `portfolio` WHERE id = $id";
        /*var_dump($sqlQuery);
        die();*/
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    public function update()
    {


        $pimg = $this->image;

        if ($pimg) {
            $dataArray = [$this->project_type, $this->project_title, $this->link, $this->image];
            $sqlQuery = "UPDATE `portfolio` SET `project_type` = ?,`project_title` = ?,`link` = ? ,`image`= ? WHERE `id` = $this->id";
        } else {
            $dataArray = [$this->project_type, $this->project_title, $this->link];
            $sqlQuery = "UPDATE `portfolio` SET `project_type` = ?,`project_title` = ?,`link` = ? WHERE `id` = $this->id";
        }

        $sth = $this->DBH->prepare($sqlQuery);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Successfully!! Data Updated...");
        } else {
            Message::message("Error!! Please check your input...");
        }
    }

    public function delete()
    {

        $sqlQuery = "DELETE from `portfolio` WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result) {

            Message::message("Successful!! Data has been deleted");
        } else {

            Message::message("Error!! ");
        }
    }

}