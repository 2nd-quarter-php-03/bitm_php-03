<?php
/**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 8/28/2018
 * Time: 9:45 PM
 */

namespace App\SkillAdmin;


use App\Message\Message;
use App\Model\Database;
use PDO;

class SkillAdmin extends Database
{


    public $id;
    public $skill_name;
    public $skill_level;
  
    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("skill_name", $postArray)) {
            $this->skill_name = $postArray['skill_name'];
        }

        if (array_key_exists("skill_level", $postArray)) {
            $this->skill_level = $postArray['skill_level'];
        }


    }// end of setData()


    public function store()
    {
        $dataArray = [$this->skill_name, $this->skill_level];
        $sqlStr = "INSERT INTO `skill` (`skill_name`,`skill_level`) VALUES (?,?)";
        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Success! Inserted Data.....");
        } else {

            Message::message("Error!! Data Does Not Inserted....");
        }

    }

    // end of store()
    public function index()
    {

        $sqlQuery = "select * from `skill`";
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    public function view($id)
    {

        $sqlQuery = "select * from `skill` WHERE id = $id";
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    public function update()
    {

        $id = $this->id;
        $skill_name = $this->skill_name;
        $skill_level = $this->skill_level;
    
        $dataArray = array($skill_name, $skill_level);
        $sqlQuery = "UPDATE `skill` SET `skill_name` = ?,`skill_level` = ? WHERE `id` = $this->id";
      

        $sth = $this->DBH->prepare($sqlQuery);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Successfully!! Data Updated...");
        } else {
            Message::message("Error!! Please check your input...");
        }
    }

    public function delete()
    {

        $sqlQuery = "DELETE from `skill` WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result) {

            Message::message("Successful!! Data has been deleted");
        } else {

            Message::message("Error!! ");
        }
    }

}