<?php

namespace App\Model;
use PDO, PDOException;

class Database {
     public $DBH;
     public function __construct()
     {
        try{
            $this->DBH =   new PDO("mysql:dbname=kdait;host=localhost", "root", "");
            //echo "Database connection successfull<br>";
        }
        catch (PDOException $error){
           echo  $error->getMessage();
        }
     }
}