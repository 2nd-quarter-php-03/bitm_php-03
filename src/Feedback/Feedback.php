<?php
/**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 8/27/2018
 * Time: 11:45 PM
 */

namespace App\Feedback;


use App\Message\Message;
use App\Model\Database;
use DateTime;
use DateTimeZone;
use PDO;
class Feedback extends Database
{

    public $id;
    public $name;
    public $email;
    public $subject;
    public $message;
    public $date;


    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("name", $postArray)) {
            $this->name = $postArray['name'];
        }

        if (array_key_exists("email", $postArray)) {
            $this->email = $postArray['email'];
        }

        if (array_key_exists("subject", $postArray)) {
            $this->subject = $postArray['subject'];
        }
        if (array_key_exists("message", $postArray)) {
            $this->message = $postArray['message'];
        }






    }// end of setData()


    public function store()
    {


        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $date=$dt->format('F j, Y, g:i a');
        $this->date = $date;
        $dataArray = [$this->date,$this->name, $this->email, $this->subject, $this->message];

        $sqlStr = "INSERT INTO feedback (`date`,`name`,`email`,`subject`,`message`) VALUES (?,?,?,?,?)";

        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Success! Inserted Data.....");
        }else{

            Message::message("Error!! Data Does Not Inserted....");
        }

    }
    // end of store()


    public function viewMessage(){


        $sqlQuery = "select * from `feedback` WHERE is_read = 0";

        $sth = $this->DBH->query($sqlQuery);


        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetchAll();

        $count = count($singleData);
        /*var_dump($count);
        die();*/

        return $count;

    }

    public function readMessage()
    {
        $sqlQuery1 = "UPDATE `feedback` SET is_read = 1";

        $sth1 = $this->DBH->query($sqlQuery1);

    }

    public function index(){

        $sqlQuery1 = "UPDATE `feedback` SET is_read = 1";

        $sth1 = $this->DBH->query($sqlQuery1);

        $sqlQuery = "select * from feedback";

        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }
    public function delete(){

        $sqlQuery = "DELETE from feedback  WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }


}