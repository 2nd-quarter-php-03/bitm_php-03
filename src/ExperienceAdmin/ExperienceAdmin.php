<?php
/**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 8/28/2018
 * Time: 9:45 PM
 */

namespace App\ExperienceAdmin;


use App\Message\Message;
use App\Model\Database;
use PDO;

class ExperienceAdmin extends Database
{


    public $id;
    public $position;
    public $company;
	 public $responsibility;
  
    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("position", $postArray)) {
            $this->position = $postArray['position'];
        }

        if (array_key_exists("company", $postArray)) {
            $this->company = $postArray['company'];
        }
		
		if (array_key_exists("responsibility", $postArray)) {
            $this->responsibility = $postArray['responsibility'];
        }


    }// end of setData()


    public function store()
    {
        $dataArray = [$this->position, $this->company, $this->responsibility];
        $sqlStr = "INSERT INTO `experience` (`position`,`company`,`responsibility`) VALUES (?,?,?)";
        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Success! Inserted Data.....");
        } else {

            Message::message("Error!! Data Does Not Inserted....");
        }

    }

    // end of store()
    public function index()
    {

        $sqlQuery = "select * from `experience`";
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    public function view($id)
    {

        $sqlQuery = "select * from `experience` WHERE id = $id";
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    public function update()
    {

        $id = $this->id;
        $position = $this->position;
        $company = $this->company;
        $responsibility = $this->responsibility;
    
        $dataArray = array($position, $company, $responsibility);
        $sqlQuery = "UPDATE `experience` SET `position` = ?,`company` = ?,`responsibility` = ? WHERE `id` = $this->id";
      

        $sth = $this->DBH->prepare($sqlQuery);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Successfully!! Data Updated...");
        } else {
            Message::message("Error!! Please check your input...");
        }
    }

    public function delete()
    {

        $sqlQuery = "DELETE from `experience` WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result) {

            Message::message("Successful!! Data has been deleted");
        } else {

            Message::message("Error!! ");
        }
    }

}