<?php


namespace App\TeamAdmin;
use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;

class TeamAdmin extends Database
{
    public $id;
    public $name;
    public $position;
    public $facebook;
    public $linkedin;
    public $image;
    public $service;
    public $title;
    public $description;

    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("name", $postArray)) {
            $this->name = $postArray['name'];
        }

        if (array_key_exists("position", $postArray)) {
            $this->position = $postArray['position'];
        }
        if (array_key_exists("facebook", $postArray)) {
            $this->facebook = $postArray['facebook'];
        }

        if (array_key_exists("linkedin", $postArray)) {
            $this->linkedin = $postArray['linkedin'];
        }
        if (array_key_exists("TeamAdmin", $postArray)) {
            $this->image = $postArray['TeamAdmin'];
        }
        

        if (array_key_exists("Services", $postArray)) {
            $this->service = $postArray['Services'];
        }

        if (array_key_exists("title", $postArray)) {
            $this->title = $postArray['title'];
        }
        if (array_key_exists("description", $postArray)) {
            $this->description = $postArray['description'];
        }



    }// end of setData()


    public function store()
        {

            $dataArray = [$this->name, $this->position, $this->facebook, $this->linkedin, $this->image];

            $sqlStr = "INSERT INTO team (`name`,`position`,`facebook`,`linkedin`,`image`) VALUES (?,?,?,?,?)";

            $sth = $this->DBH->prepare($sqlStr);
            $result = $sth->execute($dataArray);

            if ($result){

                Message::message("Success! Inserted Data.....");
               /* var_dump(Message::message("hekllo"));
                die();*/
            }else{

                Message::message("Error!! Data Does Not Inserted....");
            }

        }
    // end of store()
    public function index(){

        $sqlQuery = "select * from team";

        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }




    public function view(){

        $sqlQuery = "select * from team WHERE id = $this->id";
        /*var_dump($sqlQuery);
        die();*/
        $sth = $this->DBH->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();

        return $singleData;
    }

    public function update(){


        $id = $this->id;
        $name=$this->name ;
        $position=$this->position ;
        $facebook = $this->facebook;
        $linkedin = $this->linkedin;
        $image = $this->image;

       /* $result = "UPDATE `team` SET `name` = '$name',`position` = '$position',`facebook` = '$facebook' ,`linkedin` = '$linkedin',`image`= '$image' WHERE `id` = $this->id";
        var_dump($result);
        die();*/

       if ($image)
       {
           $dataArray = array($name,$position,$facebook,$linkedin,$image);

           $sqlQuery = "UPDATE `team` SET `name` = ?,`position` = ?,`facebook` = ? ,`linkedin` = ?,`image`= ? WHERE `id` = $this->id";

       }
       else
       {
           $dataArray = array($name,$position,$facebook,$linkedin);

           $sqlQuery = "UPDATE `team` SET `name` = ?,`position` = ?,`facebook` = ? ,`linkedin` = ? WHERE `id` = $this->id";

       }

        $sth = $this->DBH->prepare($sqlQuery);

        $result = $sth->execute($dataArray);


        if ($result){

            Message::message("Successfully!! Data Updated...");
        }else{

            Message::message("Error!! Please check your input...");
        }
    }

    public function delete(){

        $sqlQuery = "DELETE from team  WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }


  /*  public function service_store()
    {

        $dataArray = [$this->title, $this->description, $this->service];

        $sqlStr = "INSERT INTO services (`title`,`description`,`image`) VALUES (?,?,?)";

        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Success! Inserted Data.....");
        }else{

            Message::message("Error!! Data Does Not Inserted....");
        }

    }

    public function service_index(){

        $sqlQuery = "select * from services";

        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }*/


  /*-----------------------------------WEB SITE-----------------------------------*/
    public function softwareTeam(){

        $sqlQuery = "select * from team where `position` = 'Software Engineer'";

        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    public function marketingTeam(){

        $sqlQuery = "select * from team where `position` = 'Marketing Manager'";

        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    public function managementTeam(){

        $sqlQuery = "select * from team where `position` = 'Advisor' OR `position` = 'Director' OR `position` = 'Founder' OR `position` = 'Co-Founder'";

        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

}