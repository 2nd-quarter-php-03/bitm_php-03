<?php
/**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 8/28/2018
 * Time: 9:43 PM
 */

namespace App\ContactAdmin;


use App\Message\Message;
use App\Model\Database;
use DateTime;
use DateTimeZone;
use PDO;

class ContactAdmin extends Database
{
    public $id;
    public $mobile;
    public $email;
    public $address;
    public $image;
    public $link;
    public $in_time;
    public $out_time;

    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("mobile", $postArray)) {
            $this->mobile = $postArray['mobile'];
        }

        if (array_key_exists("email", $postArray)) {
            $this->email = $postArray['email'];
        }
        if (array_key_exists("address", $postArray)) {
            $this->address = $postArray['address'];
        }
        if (array_key_exists("in_time", $postArray)) {
            $this->in_time = $postArray['in_time'];
        }
        if (array_key_exists("out_time", $postArray)) {
            $this->out_time = $postArray['out_time'];
        }
        if (array_key_exists("image", $postArray)) {
            $this->image = $postArray['image'];
        }
        if (array_key_exists("link", $postArray)) {
            $this->link = $postArray['link'];
        }

    }// end of setData()


    public function store()
    {

        $dt = new DateTime($this->in_time, new DateTimezone('Asia/Dhaka'));
        $date=$dt->format('g:i a');
        $this->in_time=$date;

        $dt1 = new DateTime($this->out_time, new DateTimezone('Asia/Dhaka'));
        $date1=$dt1->format('g:i a');
        $this->out_time = $date1;


        $dataArray = [$this->mobile, $this->email, $this->address, $date,$date1,$this->link,$this->image];

        $sqlStr = "INSERT INTO contact (`mobile`,`email`,`address`,`in_time`,`out_time`,`link`,`image`) VALUES (?,?,?,?,?,?,?)";

        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Success! Inserted Data.....");
        } else {

            Message::message("Error!! Data Does Not Inserted....");
        }

    }
    // end of store()
    public function index(){

        $sqlQuery = "select * from contact";

        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function view(){

        $sqlQuery = "select * from `contact` WHERE id = $this->id";

        $sth = $this->DBH->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();

        return $singleData;
    }

    public function update(){

        $img = $this->image;
        if ($img)
        {
          /*  var_dump($img);
            die();*/
            $dataArray = [$this->mobile, $this->email, $this->address,$this->in_time,$this->out_time,$this->link,$img];

            $sqlQuery = "UPDATE `contact` SET `mobile` = ?,`email` = ?,`address` = ?,`in_time` = ?,`out_time` = ?,`link` = ?,`image` = ? WHERE `id` = $this->id";
        }
        else
        {
            $dataArray = [$this->mobile, $this->email, $this->address,$this->in_time,$this->out_time,$this->link];

            $sqlQuery = "UPDATE `contact` SET `mobile` = ?,`email` = ?,`address` = ?,`in_time` = ?,`out_time` = ?,`link` = ? WHERE `id` = $this->id";
        }

        $sth = $this->DBH->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully!! Data Updated...");
        }else{

            Message::message("Error!! Please check your input...");
        }
    }



    public function delete(){

        $sqlQuery = "DELETE from `contact` WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }
}