<?php
/**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 8/28/2018
 * Time: 9:43 PM
 */

namespace App\ProfileAdmin;


use App\Message\Message;
use App\Model\Database;
use DateTime;
use DateTimeZone;
use PDO;

class ProfileAdmin extends Database
{
    public $id;
    public $name;
    public $title;
    public $age;
    public $address;
	public $email;
    public $mobile;
    public $L_link;
    public $T_link;
    public $G_link;
    public $F_link;
    public $S_link;
    public $p_image;
    public $r_image;
  

    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

		 if (array_key_exists("name", $postArray)) {
            $this->name = $postArray['name'];
        }
		
		 if (array_key_exists("title", $postArray)) {
            $this->title = $postArray['title'];
        }
		
		 if (array_key_exists("age", $postArray)) {
            $this->age = $postArray['age'];
        }
		
		 if (array_key_exists("address", $postArray)) {
            $this->address = $postArray['address'];
        }
		
		 if (array_key_exists("email", $postArray)) {
            $this->email = $postArray['email'];
        }
		
		 if (array_key_exists("mobile", $postArray)) {
            $this->mobile = $postArray['mobile'];
        }
		
        if (array_key_exists("L_link", $postArray)) {
            $this->L_link = $postArray['L_link'];
        }

        if (array_key_exists("T_link", $postArray)) {
            $this->T_link = $postArray['T_link'];
        }
		
        if (array_key_exists("G_link", $postArray)) {
            $this->G_link = $postArray['G_link'];
        }
		
        if (array_key_exists("F_link", $postArray)) {
            $this->F_link = $postArray['F_link'];
        }
		
        if (array_key_exists("S_link", $postArray)) {
            $this->S_link = $postArray['S_link'];
        }
		
        if (array_key_exists("p_image", $postArray)) {
            $this->p_image = $postArray['p_image'];
        }
		
        if (array_key_exists("r_image", $postArray)) {
            $this->r_image = $postArray['r_image'];
        }

    }// end of setData()


    public function store()
    {

        $dataArray = [$this->name, $this->title, $this->age, $this->address, $this->email, $this->mobile, $this->L_link, $this->T_link, $this->G_link, $this->F_link,$this->S_link,$this->p_image,$this->r_image];

        $sqlStr = "INSERT INTO profile (`name`,`title`,`age`,`address`,`email`,`mobile`,`L_link`,`T_link`,`G_link`,`F_link`,`S_link`,`p_image`,`r_image`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Success! Inserted Data.....");
        } else {

            Message::message("Error!! Data Does Not Inserted....");
        }

    }
    // end of store()
    public function index(){

        $sqlQuery = "select * from profile";

        $sth = $this->DBH->prepare($sqlQuery);
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_OBJ);
        print_r($data);
die();
        //$allData = $sth->fetchAll();

        return $allData;
    }

    public function view($id){

        $sqlQuery = "select * from `profile` WHERE id = $id";

        $sth = $this->DBH->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();

        return $singleData;
    }

    public function update(){

        $pimg = $this->p_image;
        $rimg = $this->r_image;

        if ($pimg && $rimg)
        {
            $dataArray = [$this->name, $this->title, $this->age, $this->address, $this->email, $this->mobile, $this->L_link, $this->T_link, $this->G_link, $this->F_link, $this->S_link, $this->p_image, $this->r_image];

            $sqlQuery = "UPDATE `profile` SET `name` = ?,`title` = ?,`age` = ?,`address` = ?,`email` = ?,`mobile` = ?, `L_link` = ?,`T_link` = ?,`G_link` = ?,`F_link` = ?,`S_link` = ?,`p_image` = ?,`r_image` = ? WHERE `id` = $this->id";
        }
        else if($pimg && !$rimg)
        {
            $dataArray = [$this->name, $this->title, $this->age, $this->address, $this->email, $this->mobile, $this->L_link, $this->T_link, $this->G_link, $this->F_link, $this->S_link, $this->p_image];

            $sqlQuery = "UPDATE `profile` SET `name` = ?,`title` = ?,`age` = ?,`address` = ?,`email` = ?,`mobile` = ?,`L_link` = ?,`T_link` = ?,`G_link` = ?,`F_link` = ?,`S_link` = ?,`p_image` = ? WHERE `id` = $this->id";
        }
        else if(!$pimg && $rimg)
        {
            $dataArray = [$this->name, $this->title, $this->age, $this->address, $this->email, $this->mobile, $this->L_link, $this->T_link, $this->G_link, $this->F_link, $this->S_link, $this->r_image];

            $sqlQuery = "UPDATE `profile` SET `name` = ?,`title` = ?,`age` = ?,`address` = ?,`email` = ?,`mobile` = ?,`L_link` = ?,`T_link` = ?,`G_link` = ?,`F_link` = ?,`S_link` = ?,`r_image` = ? WHERE `id` = $this->id";
        }
        else
        {
            $dataArray = [$this->name, $this->title, $this->age, $this->address, $this->email, $this->mobile, $this->L_link, $this->T_link, $this->G_link, $this->F_link, $this->S_link];

            $sqlQuery = "UPDATE `profile` SET `name` = ?,`title` = ?,`age` = ?,`address` = ?,`email` = ?,`mobile` = ?,`L_link` = ?,`T_link` = ?,`G_link` = ?,`F_link` = ?,`S_link` = ? WHERE `id` = $this->id";
        }

        $sth = $this->DBH->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully!! Data Updated...");
        }else{

            Message::message("Error!! Please check your input...");
        }
    }



    public function delete(){

        $sqlQuery = "DELETE from `profile` WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }
}