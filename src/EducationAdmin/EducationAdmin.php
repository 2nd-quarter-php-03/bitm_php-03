<?php
/**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 8/28/2018
 * Time: 9:45 PM
 */

namespace App\EducationAdmin;


use App\Message\Message;
use App\Model\Database;
use PDO;

class EducationAdmin extends Database
{


    public $id;
    public $degree_name;
    public $institute_name;
  
    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("degree_name", $postArray)) {
            $this->degree_name = $postArray['degree_name'];
        }

        if (array_key_exists("institute_name", $postArray)) {
            $this->institute_name = $postArray['institute_name'];
        }


    }// end of setData()


    public function store()
    {
        $dataArray = [$this->degree_name, $this->institute_name];
        $sqlStr = "INSERT INTO `education` (`degree_name`,`institute_name`) VALUES (?,?)";
        $sth = $this->DBH->prepare($sqlStr);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Success! Inserted Data.....");
        } else {

            Message::message("Error!! Data Does Not Inserted....");
        }

    }

    // end of store()
    public function index()
    {

        $sqlQuery = "select * from `education`";
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    public function view($id)
    {

        $sqlQuery = "select * from `education` WHERE id = $id";
        $sth = $this->DBH->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    public function update()
    {

        $id = $this->id;
        $degree_name = $this->degree_name;
        $institute_name = $this->institute_name;
    
        $dataArray = array($degree_name, $institute_name);
        $sqlQuery = "UPDATE `education` SET `degree_name` = ?,`institute_name` = ? WHERE `id` = $this->id";
      

        $sth = $this->DBH->prepare($sqlQuery);
        $result = $sth->execute($dataArray);

        if ($result) {

            Message::message("Successfully!! Data Updated...");
        } else {
            Message::message("Error!! Please check your input...");
        }
    }

    public function delete()
    {

        $sqlQuery = "DELETE from `education` WHERE `id` =$this->id";

        $result = $this->DBH->exec($sqlQuery);

        if ($result) {

            Message::message("Successful!! Data has been deleted");
        } else {

            Message::message("Error!! ");
        }
    }

}