-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 12:37 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kdait`
--
CREATE DATABASE IF NOT EXISTS `kdait` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kdait`;

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `name`, `password`) VALUES
(11, 'admin', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(50) NOT NULL,
  `mobile` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `link` varchar(500) NOT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `mobile`, `email`, `address`, `in_time`, `out_time`, `link`, `image`) VALUES
(12, '+88 01872 637 800-14', 'info@kdait.com', 'Ground floor, House # 86, \r\nRoad # 04, Block # C, \r\nSugandha City Corp. R/A, Panchlaish,\r\n Chittagong, Bangladesh', '09:30:00', '05:30:00', 'https://www.google.com/maps/@22.3638084,91.8242543,20.79z', '1535778849kdaitmap.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `degree_name` varchar(150) NOT NULL,
  `institute_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `degree_name`, `institute_name`) VALUES
(1, 'B.Sc in Computer Science and Engineering', 'IIUC '),
(2, 'M.Sc in Computer Science and Engineering', 'IIUC'),
(3, 'PhD', 'IIUC');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `id` int(50) NOT NULL,
  `position` varchar(150) NOT NULL,
  `company` varchar(150) NOT NULL,
  `responsibility` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`id`, `position`, `company`, `responsibility`) VALUES
(1, 'Internship', 'Code Station', 'Analyzing Information , General Programming Skills, Software Design, Software Debugging, Software Documentation, Software Testing, Problem Solving, Teamwork, Software Development Fundamentals, Software Development Process, Software Requirements'),
(2, 'Jr. Software Engineer', 'KDA IT ', 'Analyzing Information , General Programming Skills, Software Design, Software Debugging, Software Documentation, Software Testing, Problem Solving, Teamwork, Software Development Fundamentals, Software Development Process, Software Requirements');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `date` varchar(200) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `date`, `name`, `email`, `subject`, `message`, `is_read`) VALUES
(15, 'August 30, 2018, 3:46 pm', 'Yusuf', 'yusuf@gmail.com', 'Feedback', 'Hi', 1),
(16, 'October 31, 2018, 3:18 pm', 'Arif', 'aparif0@gmail.com', 'ggg', 'ggg', 1),
(17, 'October 31, 2018, 3:19 pm', 'Arif', 'aparif0@gmail.com', 'hnmjhvb', 'wrfef', 1),
(18, 'October 31, 2018, 3:20 pm', 'Arif', 'aparif0@gmail.com', 'hnmjhvb', 'ddd', 1),
(19, 'November 1, 2018, 3:45 pm', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(50) NOT NULL,
  `project_type` varchar(150) NOT NULL,
  `project_title` varchar(150) NOT NULL,
  `link` varchar(150) NOT NULL,
  `image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `project_type`, `project_title`, `link`, `image`) VALUES
(1, 'PHP', 'Hospital Management', 'http://kdait.com', 'teamwork.jpg'),
(2, 'Laravel', 'School Management', 'http://demo1.bangladeshbusinessdirectory.com/', '1541067277Ocean-freight-guide-from-China-forwarding.jpg'),
(3, 'WordPress', 'Hotel Management', 'http://demo1.bangladeshbusinessdirectory.com/', '1541067346teamwork.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(50) NOT NULL,
  `name` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `age` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `L_link` varchar(150) NOT NULL,
  `T_link` varchar(150) NOT NULL,
  `G_link` varchar(150) NOT NULL,
  `F_link` varchar(150) NOT NULL,
  `S_link` varchar(150) NOT NULL,
  `p_image` varchar(255) NOT NULL,
  `r_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `name`, `title`, `age`, `address`, `email`, `mobile`, `L_link`, `T_link`, `G_link`, `F_link`, `S_link`, `p_image`, `r_image`) VALUES
(1, 'Arifur Rahman', 'Web Development', '25', 'ctg', 'aparif0@gmail.com', '01 673348095', ' https://www.linkedin.com/in/md-arifur-rahman-2905a7158/', 'https://twitter.com/aparifurrahman', 'https://gitlab.com/Arif520', 'https://www.facebook.com/Arif.IIUC', 'Arif12345', 'arif420.jpg', 'Arifur_Rahman_Software_Engineer.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE `skill` (
  `id` int(50) NOT NULL,
  `skill_name` varchar(150) NOT NULL,
  `skill_level` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skill`
--

INSERT INTO `skill` (`id`, `skill_name`, `skill_level`) VALUES
(2, 'PHP', '90'),
(3, 'Laravel', '80'),
(4, 'JavaScript', '70'),
(5, 'HTML5', '95'),
(6, 'CSS3', '80'),
(7, 'Bootstrap', '90'),
(8, 'MySQL', '90'),
(9, 'AngularJS', '60'),
(10, 'WordPress', '60');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `skill`
--
ALTER TABLE `skill`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
