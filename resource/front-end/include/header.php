<header>

    <div class="header-two-area" id="sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="logo-area" >
                        <a href="index.php" ><img src="../../resource/front-end/images/kdait.png" alt=""></a>
                    </div>
                </div>

                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">


                    <div class="main-menu2-area">
                        <nav>
                            <ul>
                                <li class="#"><a href="index.php">Home </a>
                                </li>

                                <li><a href="about.php">About Us</a></li>

                                <li><a href="services.php">Services <i class="" aria-hidden="true"></i></a>

                                </li>




                                <li><a href="training.php">Training <i class="" aria-hidden="true"></i></a>

                                </li>

                                <li><a href="gallery.php">Gallery</i></a>

                                <li><a href="#">Team <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul>
                                        <li><a href="software-team.php">Software Team</a></li>
                                        <li><a href="marketing-team.php">Marketing Team</a></li>
                                        <li><a href="management-team.php">Management Team</a></li>
                                    </ul>
                                </li>

                                </li>
                                <li><a href="contact.php">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- mobile-menu-area start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li class="active"><a href="index.php">Home</a></li>

                                <li><a href="about.php">About Us</a></li>

                                <li><a href="services.php">Services <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul>
                                        <li><a href="single-services.php">Mobile Apps Development</a></li>
                                        <li><a href="single-services.php">Software Development</a></li>
                                        <li><a href="single-services.php">Web Design & Development</a></li>
                                        <li><a href="single-services.php">IoT Based Solutions</a></li>
                                        <li><a href="single-services.php">ERP Solutions</a></li>
                                        <li><a href="single-services.php">E-commerce Solution</a></li>

                                    </ul>
                                </li>

                                <li><a href="training.php">Training <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul>
                                        <li><a href="training1-service.php">Mobile Apps Development</a></li>
                                        <li><a href="training2-services.php">Web Development</a></li>
                                        <li><a href="training3-services.php">Responsive Web Design </a></li>
                                        <li><a href="training4-services.php">Programming Language</a></li>
                                    </ul>
                                </li>

                                <li><a href="team.php">Team <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul>
                                        <li><a href="team1.php">Software Team</a></li>
                                        <li><a href="team2.php">Marketing Team</a></li>
                                        <li><a href="team3.php">Management Team</a></li>
                                    </ul>
                                </li>
                                <li><a href="gallery.php">Gallery</a>
                                </li>

                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area end -->


</header>
<!-- Header Two End Here -->