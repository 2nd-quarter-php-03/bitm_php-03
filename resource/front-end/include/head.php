<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>KDA | IT</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="#">
    <!-- favicon-->
    <link rel="shortcut icon" type="image/x-icon" href="../../resource/front-end/images/favicon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="../../resource/front-end/css/bootstrap.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="../../resource/front-end/css/animate.css">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="../../resource/front-end/css/jquery-ui.min.css">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="../../resource/front-end/css/meanmenu.min.css">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="../../resource/front-end/css/owl.carousel.css">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="../../resource/front-end/css/font-awesome.min.css">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="../../resource/front-end/inc/custom-slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="../../resource/front-end/inc/custom-slider/css/preview.css" type="text/css" media="screen" />
    <!-- fancybox CSS -->
    <link rel="stylesheet" href="../../resource/front-end/inc/fancybox/jquery.fancybox.css">
    <!-- style css -->
    <link rel="stylesheet" href="style.css">
    <!-- CSS Color Plate -->
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/skype-color.css">
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/red-color.css">
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/green-color.css">
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/orange-color.css">
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/coral-color.css">
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/deeplink-color.css">
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/khaki-color.css">
    <link rel="stylesheet" href="../../resource/front-end/inc/multicolor-css/greenyellow-color.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="../../resource/front-end/css/responsive.css">
    <!-- modernizr css -->
    <script src="../../resource/front-end/js/vendor/modernizr-2.8.3.min.js"></script>

</head>

<body>
<div class="demo-panel-setting-area">
    <div class="cross-button"><i class="fa fa-cog fa-spin" aria-hidden="true"></i></div>
    <h3>Multicolor Setting:</h3>
    <div class="color-scheme-area">
        <span class="skype-color"></span>
        <span class="red-color"></span>
        <span class="green-color"></span>
        <span class="orange-color"></span>
        <span class="coral-color"></span>
        <span class="deeppink-color"></span>
        <span class="khaki-color"></span>
        <span class="greenyellow-color"></span>
    </div>
</div>
<!--<div class="header-top-area">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-sm-6 text-left" >-->
<!--                <div class="header-top-left">-->
<!--                    <div class="social-media">-->
<!--                        <ul style="alignment: center">-->
<!--                            <li><a href="#"><i aria-hidden="true" ></i><h4>Where Dreams Become Reality</h4></a></li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-sm-6 text-right">-->
<!--                <div class="header-top-right">-->
<!--                    <ul>-->
<!--                        <li><i class="fa fa-envelope-o" aria-hidden="true" ></i> kdait2018@gmail.com</li>-->
<!--                        <li><i class="fa fa-phone" aria-hidden="true"></i>  +880 </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- Header Two Start Here -->