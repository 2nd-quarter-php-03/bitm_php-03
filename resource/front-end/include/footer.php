<footer>
    <div class="footer-top-area2">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="widget">
                        <div class="footer-logo">
                            <a href="index.php"><img src="../../resource/front-end/images/kdait.png" alt="image"></a>
                        </div>
                        <p><strong>"Where Dreams Become Reality"</strong> </p>
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="widget">
                        <h2 class="widget-title">Help & Suppoert Center</h2>
                        <div class="menu-header-container">
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Privacy policy</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="widget">
                        <h2 class="widget-title">Tags</h2>
                        <div class="popular-tags">
                            <ul>
                                <li><a href="#">Responsive Design</a></li>
                                <li><a href="#"> Software Development</a></li>
                                <li><a href="#">PSD Design</a></li>
                                <li><a href="#">Creative Design</a></li>
                                <li><a href="#">Support Team</a></li>
                                <li><a href="#">Help</a></li>
                                <li><a href="#">SEO</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright &copy; 2018<a href="http://kdait.com/">KDA IT Items</a> All Right Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- all js here -->
<!-- jquery latest version -->
<script src="../../resource/front-end/js/vendor/jquery-1.12.0.min.js"></script>
<!-- bootstrap js -->
<script src="../../resource/front-end/js/bootstrap.min.js"></script>
<!-- owl.carousel js -->
<script src="../../resource/front-end/js/owl.carousel.min.js"></script>
<!-- meanmenu js -->
<script src="../../resource/front-end/js/jquery.meanmenu.js"></script>
<!-- jquery-ui js -->
<script src="../../resource/front-end/js/jquery-ui.min.js"></script>
<!-- wow js -->
<script src="../../resource/front-end/js/wow.min.js"></script>
<!-- plugins js -->
<script src="../../resource/front-end/js/plugins.js"></script>
<!-- Nivo slider js-->
<script src="../../resource/front-end/inc/custom-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="../../resource/front-end/inc/custom-slider/home.js" type="text/javascript"></script>
<!-- jquery.counterup js -->
<script src="../../resource/front-end/js/jquery.counterup.min.js"></script>
<script src="../../resource/front-end/js/waypoints.min.js"></script>
<!-- knob circle js -->
<script src="../../resource/front-end/js/jquery.knob.js"></script>
<!-- jquery.appear js -->
<script src="../../resource/front-end/js/jquery.appear.js"></script>
<!-- jquery.fancybox.pack js -->
<script src="../../resource/front-end/inc/fancybox/jquery.fancybox.pack.js"></script>
<!-- jQuery MixedIT Up -->
<script src="../../resource/front-end/js/jquery.mixitup.min.js" type="text/javascript"></script>
<!-- main js -->
<script src="../../resource/front-end/js/main.js"></script>
</body>

</html>