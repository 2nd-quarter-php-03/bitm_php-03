<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once "../../src/SlideAdmin/SlideAdmin.php";
require_once "../../src/PortfolioAdmin/PortfolioAdmin.php";
require_once ("../../src/Message/Message.php");

use App\Message\Message;
use App\SlideAdmin\SlideAdmin;
use App\PortfolioAdmin\PortfolioAdmin;

$msg = Message::message();

$object1 = new SlideAdmin();
$allData1 = $object1->index();

$object = new PortfolioAdmin();
$allData = $object->index();

?>

<?php include 'include/head.php';?>
<?php include 'include/header.php';?>

        <!-- Slider area-->
        <div class="slider-area slider-area-2">
            <div class="bend niceties preview-2">
                <div id="ensign-nivoslider" class="slides">

        <?php
            $serial1 = 1;
            foreach ($allData1 as $record1)
            {

                echo "<img src='../admin/SlideFiles/$record1->image' alt='' title='#slider-direction-$serial1'/>";

                $serial1++;
                if($serial1 > 3){$serial1 = $serial1 % 3;}
            }
        ?>

        </div>




                <!-- direction 2 -->
                <div id="slider-direction-1" class="slider-direction">
                    <div class="slider-content t-lfl s-tb slider-2">
                        <div class="title-container s-tb-c">
                            <h1 class="title1">KDA <span>IT</span></h1>
                            <div class="title2">Software Development| Mobile Apps Development| IoT Based Solutions | ERP Solutions</div>
                            <div class="slider-botton" >
                                <ul>
                                    <li class="acitve"><a href="#">Where Dreams Become Reality</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>  
                </div>

                <!-- direction 2 -->
                <div id="slider-direction-2" class="slider-direction">
                    <div class="slider-content t-cn s-tb slider-2">
                        <div class="title-container s-tb-c">
                            <h1 class="title1">Service On <span>KDA IT</span></h1>
                            <div class="title2">Software Development| Mobile Apps Development| IoT Based Solutions | ERP Solutions</div>
                            <div class="slider-botton" >
                                <ul>
                                    <li class="acitve"><a href="#">Read More</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>  
                </div>
                <!-- direction 2 -->
                <div id="slider-direction-3" class="slider-direction">
                    <div class="slider-content t-cn s-tb slider-2">
                        <div class="title-container s-tb-c">
                            <h1 class="title1"><span>KDA IT</span> is Powerful Team</h1>
                            <div class="title2" >Software Development| Mobile Apps Development| IoT Based Solutions | ERP Solutions</div>
                            <div class="slider-botton" >
                                <ul>
                                    <li class="acitve"><a href="#">Learn More</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <!-- Slider end-->
        <!-- Services Start Here -->  
        <div class="service-area">
             <div class="container">
                 <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">                        
                        <div class="section-area2">
                            <h2>WHAT WE  <span>OFFER</span></h2>
                            <p>KDA IT Provides IT Consultation, Software Development, Smart E-commerce Solution, Web Design
                                and Development, Android Application Development and also IT Training.
                            </p>
                        </div>
                    </div>                          
                 </div>

                 <div class="row padding-top1">
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                         <div class="single-service">
                             <div class="services-icon">
                                 <a href="app-services.php"><i class="fa fa-cc-diners-club" aria-hidden="true"></i></a>
                             </div>
                             <h3><a href="app-services.php">MOBILE APPS DEVELOPMENT</a></h3>
                             <p>Our Android Developers Can Provide Any type of Android Solution.</p>
                         </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                         <div class="single-service">
                             <div class="services-icon">
                                 <a href="software-services.php"><i class="fa fa-cc-diners-club" aria-hidden="true"></i></a>
                             </div>
                             <h3><a href="software-services.php">SOFTWARE DEVELOPMENT</a></h3>
                             <p>We provide software solution that meets your objective.</p>
                         </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                         <div class="single-service">
                             <div class="services-icon">
                                 <a href="web-service.php"><i class="fa fa-cc-diners-club" aria-hidden="true"></i></a>
                             </div>
                             <h3><a href="web-service.php">WEB DESIGN & DEVELOPMENT</a></h3>
                             <p>We desing and develop standard web solution for you.</p>
                         </div>
                     </div>
                 </div><br><br>

                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                     <div class="single-service">
                         <div class="services-icon">
                             <a href="iot-services.php"><i class="fa fa-flask" aria-hidden="true"></i></a>
                         </div>
                         <h3><a href="iot-services.php">IOT BASED SOLUTIONS</a></h3>
                         <p>We deliver complex IoT projects across multiple domains, including Automotive,
                             Manufacturing, Healthcare, and Smart City.</p>
                     </div>
                 </div>

                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                     <div class="single-service">
                         <div class="services-icon">
                             <a href="ecommerce-services.php"><i class="fa fa-flask" aria-hidden="true"></i></a>
                         </div>
                         <h3><a href="ecommerce-services.php">E-COMMERCE SOLUTION</a></h3>
                         <p>So you have a local business and you are ready to take it to the next level ?
                             We provide high quality web solution.</p>
                     </div>
                 </div>

                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                     <div class="single-service">
                         <div class="services-icon">
                             <a href="erp-services.php"><i class="fa fa-flask" aria-hidden="true"></i></a>
                         </div>
                         <h3><a href="erp-services.php">ERP SOLUTIONS</a></h3>
                         <p>KDA IT’s ERP solutions are committed to delivering digital
                             integration of a company’s activities in one place.</p>
                     </div>
                 </div>

             </div>
         </div>     
        <!-- Services End Here -->

        <!-- Home Two about Us Area Start Here -->   
        <div class="home2-about-us">
                <div class="container-fluid acurate">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="about-us-image">
                                <img src="../../resource/front-end/images/about.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="about-content">
                                <h2>ABOUT <span>Us</span></h2>

                                <span>KDA IT has started its journey as a professional IT company from 15th April 2018.</span>

<!--                                <p>First one is quality project development and second is priority to customer satisfaction and quality support to them.-->
<!--                                    Day by day we have enriched ourselves by strengthening capacity, expertise-->
<!--                                    manpower, resources, service and product line with a long term vision.</p>-->
<!---->
<!--                                <p>Our worldwide operational and performance standards translate the corporate values into specific management expectations.-->
<!--                                    We preserve a high level of business ethics characterized by integrity and honesty in all our business actions.</p>-->

                                <strong>MISSION AND VISION</strong>
                                <p>Our Mission & Vision declares our purpose as a Company and serves as a standard against which we weigh our actions and decisions.</p>
                                <strong>“We help our clients achieve their Business and Financial objectives by providing Technology and
                                    Process Outsourcing Solutions. Since we have a deep investment in IP-oriented offerings, our clients
                                    ‘Get to Market’ Faster, Save Money and Reduce Risk.”</strong><br>
                                <strong>“To create a domain-centric approach to become true value partners to our clients globally.”</strong><br>
                                <div class="see-more">
                                    <a href="about.php">See More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        <!-- Home Two about Us Area End Here -->




        <!-- Portfolio Section Start Here -->
        <div class="portfolio-area portfolio-area2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">                        
                        <div class="section-area2">
                            <h2>OUR AWESOME <span>PORTFOLIO</span></h2>
                            <p>KDA IT is dedicated to increase the professionalism to the dedicated field .</p>
                        </div>
                    </div>                    
                </div>


                <div class="row padding-top">




                        <?php
                        $serial =1;
                        foreach ($allData as $record)
                        {

                            echo "   
   
                       <div class='col-lg-3 col-md-6 col-sm-6 col-xs-12 acurate'>
                        <div class='single-portfolio'>
                            
                            <div class='portfolio-image'>  
                                  <input type=\"hidden\" name=\"id\" value=\"<?php echo $record->id?>\">              
                                <a href='single-portfolio.php'><img src='../admin/PortfolioUploads/$record->image' alt=''></a>
                            </div>
                            <div class='overley'>
                                <div class='portfolio-details'>
                                    
                                    <h3><a href='single-portfolio.php?id=$record->id'>$record->name</a></h3>
                                </div>
                            </div>
             
                      </div>
                        </div>               
                            ";}?>



                </div>
            </div>
        </div>
        <!-- Portfolio Section End Here -->




        <!-- Blog Area End Here -->
        <!-- Footer Start Here -->

        <?php include 'include/footer.php';?>


