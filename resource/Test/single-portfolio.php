<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once "../../src/PortfolioAdmin/PortfolioAdmin.php";
require_once ("../../src/Message/Message.php");

use App\Message\Message;
use App\Utility\Utility;

use App\PortfolioAdmin\PortfolioAdmin;
$msg = Message::message();
$object = new PortfolioAdmin();
$object->setData($_GET);
$allData = $object->view();

?>
<?php include 'include/head.php';?>
<?php include 'include/header.php';?>

        <!-- Header Area End Here -->
        <!-- Page Header Section breadcumb Start Here -->
        <div class="page-header-area" style="background-image: url('../../resource/front-end/images/bennar.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <div class="header-page">
                            <h1>Single Portfolio</h1>
                            <ul>
                                <li> <a href="index.php">Home</a> </li>
                                <li>Single Portfolio</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header Section breadcumb End Here -->
        <!-- Portfolio Details Start Here -->
        <div class="portfolio-details-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <div class="portfolio-image">
                            <img src="../admin/PortfolioUploads/<?php echo $allData->image?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="row padding-top">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="project-description">
                            <h3><?php echo "$allData->name" ?></h3>
                            <p><?php echo "$allData->description" ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="portfolio-informations">
                        <div class="visit-project">
                            <a href="<?php echo $allData->link ?>" target="_blank">Visit Project</a>
                        </div>

                    </div>
                </div>


            </div>
        </div>
        <!-- Portfolio Details End Here -->

<?php include 'include/footer.php';?>


