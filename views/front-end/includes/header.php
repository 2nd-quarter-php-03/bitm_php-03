<!-- Header -->
<header class="header header-hidden z-depth-1 shadow-change">
    <div class="site-logo"><a href=""><?php echo $allData[0]->name; ?></a></div>
    <div class="menu-bar btn-floating waves-effect waves-light"><span class="fa fa-bars"></span></div>
    <div class="search-open btn-floating waves-effect waves-light"><span class="fa fa-search"></span></div>
    <div class="search-area search-area-hidden clearfix">
        <div class="search-input">
            <form>
                <fieldset>
                    <input type="search" placeholder="Type Here & Hit Enter...">
                </fieldset>
            </form>
        </div>
    </div>
    <nav class="main-nav">
        <ul>
            <li><a href="#0" class="animatescroll-link waves-effect" onclick="$('html').animatescroll();">Home</a></li>
<!--            <li><a href="#0" class="testimonial-section-nav animatescroll-link waves-effect" onclick="$('#testimonial-section').animatescroll();">About Me</a></li>-->
            <li><a href="#0" class="skill-section-nav animatescroll-link waves-effect" onclick="$('#skill-section').animatescroll();">Skills</a></li>
            <li><a href="#0" class="education-section-nav animatescroll-link waves-effect" onclick="$('#education-section').animatescroll();">Education</a></li>
            <li><a href="#0" class="experience-section-nav animatescroll-link waves-effect" onclick="$('#experience-section').animatescroll();">Experience</a></li>
            <li><a href="#0" class="portfolio-section-nav animatescroll-link waves-effect" onclick="$('#portfolio-section').animatescroll();">Portfolio</a></li>
<!--            <li><a href="#0" class="client-section-nav animatescroll-link waves-effect" onclick="$('#client-section').animatescroll();">Client</a></li>-->
<!--            <li><a href="#0" class="blog-section-nav animatescroll-link waves-effect" onclick="$('#blog-section').animatescroll();">Blog</a></li>-->
<!--            <li><a href="#0" class="pricing-section-nav animatescroll-link waves-effect" onclick="$('#pricing-section').animatescroll();">Pricing</a></li>-->
            <li><a href="#0" class="contact-section-nav animatescroll-link waves-effect" onclick="$('#contact-section').animatescroll();">Contact</a></li>
        </ul>
    </nav>
</header>
<!--/ Header -->