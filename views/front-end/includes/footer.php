
<!-- Contact Section -->

<!--/ Contact Section -->

<!-- Footer Section -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <a class="btn btn-floating waves-effect waves-light back-to-top animatescroll-link" onclick="$('html').animatescroll();" href="#0">
                    <span class="fa fa-angle-up"></span>
                </a>
                <div class="social-links">
                    <a class="waves-effect waves-light" href="<?php echo $allData[0]->F_link; ?>"><span class="fa fa-facebook"></span></a>
                    <a class="waves-effect waves-light" href="<?php echo $allData[0]->T_link; ?>"><span class="fa fa-twitter"></span></a>
                    <a class="waves-effect waves-light" href=><span class="fa fa-google-plus"></span></a>
                    <a class="waves-effect waves-light" href="<?php echo $allData[0]->L_link; ?>"><span class="fa fa-linkedin"></span></a>
                    <a class="waves-effect waves-light" href="<?php echo $allData[0]->G_link; ?>"><span class="fa fa-github"></span></a>
                    <a class="waves-effect waves-light" href="<?php echo $allData[0]->S_link; ?>"><span class="fa fa-skype"></span></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ Footer Section -->

<!-- Color Scheme
<div class="color-scheme-select">
    <div class="color-scheme-title">
        20 Awesome Colors
    </div>
    <div id="color-1" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#0EB57D"></div>
    <div id="color-2" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#2196F3"></div>
    <div id="color-3" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF1902"></div>
    <div id="color-4" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF9800"></div>
    <div id="color-5" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#E91E63"></div>
    <div id="color-6" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#009688"></div>
    <div id="color-7" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF5722"></div>
    <div id="color-8" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#9EC139"></div>
    <div id="color-9" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#9C27B0"></div>
    <div id="color-10" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#4CAF50"></div>
    <div id="color-11" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#795548"></div>
    <div id="color-12" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF007F"></div>
    <div id="color-13" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#673AB7"></div>
    <div id="color-14" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#8BC34A"></div>
    <div id="color-15" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#3E2723"></div>
    <div id="color-16" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF7711"></div>
    <div id="color-17" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#BF9C4F"></div>
    <div id="color-18" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#33691E"></div>
    <div id="color-19" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#607D8B"></div>
    <div id="color-20" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF7077"></div>
    <div class="color-scheme-select-btn">
        <span class="fa fa-cog"></span>
    </div>
</div>
Color Scheme -->

<script type="text/javascript" src="../../resource/front-end/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/animatescroll.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/materialize.min.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/device.min.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/isotope.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/magnific-popup.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/owl.carousel.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/smoothscroll.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/validator.min.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/waypoints.min.js"></script>
<script type="text/javascript" src="../../resource/front-end/js/wow.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA2s8Dc-tOHNpSnwh9YvdH-jcU5Jh1h_UY"></script>
<script type="text/javascript" src="../../resource/front-end/js/custom.js"></script>
</body>


</html>
