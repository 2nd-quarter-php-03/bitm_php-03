<!DOCTYPE html>
<html lang="en">


<head>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="UTF-8">
    <meta name="author" content="#">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Engr.Arif</title>
<!--    <link rel="shortcut icon" href="../../resource/front-end/img/favicon.png"/>-->
<!--    <link rel="apple-touch-icon" href="../../resource/front-end/img/apple-touch-icon.png"/>-->
<!--    <link rel="apple-touch-icon" sizes="72x72" href="../../resource/front-end/img/apple-touch-icon-72x72.png"/>-->
<!--    <link rel="apple-touch-icon" sizes="114x114" href="../../resource/front-end/img/apple-touch-icon-114x114.png"/>-->
<!--    <link rel="apple-touch-icon" sizes="144x144" href="../../resource/front-end/img/apple-touch-icon-144x144.png"/>-->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,900italic,900,700italic,700,500italic,500,400italic">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/font/font-awesome/css/font-awesome.min.css"/>
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/animate.css">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/magnific-popup.css">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/owl.carousel.css">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/owl.theme.css">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/owl.transitions.css">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/style.css">
    <link class="color-scheme" type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/color-1.css">
    <link type="text/css" rel="stylesheet" media="all" href="../../resource/front-end/css/responsive.css">
    <script type="text/javascript" src="../../resource/front-end/js/modernizr.js"></script>
</head>
<body>

<!--<!-- Preloader -->
<!--<div class="preloader">-->
<!--    <div class="preloader-inner">-->
<!--        <div class="preloader-wrapper active">-->
<!--            <div class="spinner-layer">-->
<!--                <div class="circle-clipper left">-->
<!--                    <div class="circle"></div>-->
<!--                </div>-->
<!--                <div class="gap-patch">-->
<!--                    <div class="circle"></div>-->
<!--                </div>-->
<!--                <div class="circle-clipper right">-->
<!--                    <div class="circle"></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<!--/ Preloader -->
