
<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once "../../src/ProfileAdmin/ProfileAdmin.php";
require_once "../../src/SkillAdmin/SkillAdmin.php";
require_once "../../src/PortfolioAdmin/PortfolioAdmin.php";
require_once "../../src/EducationAdmin/EducationAdmin.php";
require_once "../../src/ExperienceAdmin/ExperienceAdmin.php";
require_once "../../src/Feedback/Feedback.php";
require_once ("../../src/Message/Message.php");

use App\Message\Message;
use App\ProfileAdmin\ProfileAdmin;
use App\SkillAdmin\SkillAdmin;
use App\PortfolioAdmin\PortfolioAdmin;
use App\EducationAdmin\EducationAdmin;
use App\ExperienceAdmin\ExperienceAdmin;
use App\Feedback\Feedback;


$msg = Message::message();

$object = new ProfileAdmin();
$allData = $object->index();

$object2 = new SkillAdmin();
$allData2 = $object2->index();

$object3 = new PortfolioAdmin();
$allData3 = $object3->index();

$object4 = new EducationAdmin();
$allData4 = $object4->index();

$object5 = new ExperienceAdmin();
$allData5 = $object5->index();

$object6 = new Feedback();
$allData6 = $object5->index();
?>


<?php include 'includes/head.php'; ?>
<?php include 'includes/header.php'; ?>

		<!-- Top Section -->
		<div class="site-header z-depth-1 top-section">
			<div class="container">
				<div class="row">
					<div class="col l6 m6 s12 pd-0">
						<div class="site-header-title">
							 <?php echo $allData[0]->name; ?>
							<span><?php echo $allData[0]->title; ?></span>
							<span></span>
						</div>
					</div>
					<div class="col l6 m6 s12 pd-0">
						<div class="site-header-contact">
							<a class="btn btn-floating waves-effect waves-light" href="<?php echo $allData[0]->F_link; ?>"><span class="fa fa-facebook"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="<?php echo $allData[0]->T_link; ?>"><span class="fa fa-twitter"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href=><span class="fa fa-google-plus"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="<?php echo $allData[0]->L_link; ?>"><span class="fa fa-linkedin"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="<?php echo $allData[0]->G_link; ?>"><span class="fa fa-github"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="<?php echo $allData[0]->S_link; ?>"><span class="fa fa-skype"></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ Top Section -->

		<!-- Overview Section -->
		<section id="overview-section" class="overview-section">
			<div class="section-content">
				<div class="container">
					<div class="row">
						<div class="col s12 about-section w-block z-depth-1 shadow-change pd-0 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">
							<div class="col l5 m6 s12 about-img pd-0 image-bg" data-image-bg="../admin/Profile/ProfileFiles/<?php echo $allData[0]->p_image; ?>">
								<div class="about-more">
									<div class="about-more-content">
										<a class="btn btn-floating btn-large tooltipped" href="../admin/Profile/ProfileFiles/<?php echo $allData[0]->r_image; ?>" target="_blank" data-position="top" data-delay="50" data-tooltip="Download Resume"><span class="fa fa-download"></span></a>
									</div>
								</div>
							</div>
							<div class="col l7 m6 s12 about-data pd-0">
								<div class="about-desc pd-30">
									<div class="arrow-box">Hello & Welcome</div>
									<div class="overview-name">I'm <span><?php echo $allData[0]->name; ?></span></div>
									<div class="overview-title"><?php echo $allData[0]->title; ?></div>
									<div class="overview-data">
										<div><span>Age</span><?php echo $allData[0]->age; ?></div>
										<div><span>Address</span><?php echo $allData[0]->address; ?></div>
										<div><span>Email</span><?php echo $allData[0]->email; ?></div>
										<div><span>Phone</span>+88<?php echo $allData[0]->mobile; ?></div>
										<div><span></span>+8801 673348095</div>
									</div>
								</div>
								<div class="about-social col s12 pd-0">
									<a class="waves-effect waves-light col s2 pd-0" href="<?php echo $allData[0]->F_link; ?>"><span class="fa fa-facebook"></span></a>
									<a class="waves-effect waves-light col s2 pd-0" href="<?php echo $allData[0]->T_link; ?>"><span class="fa fa-twitter"></span></a>
									<a class="waves-effect waves-light col s2 pd-0" href="#"><span class="fa fa-google-plus"></span></a>
									<a class="waves-effect waves-light col s2 pd-0" href="<?php echo $allData[0]->L_link; ?>"><span class="fa fa-linkedin"></span></a>
									<a class="waves-effect waves-light col s2 pd-0" href="<?php echo $allData[0]->G_link; ?>"><span class="fa fa-github"></span></a>
									<a class="waves-effect waves-light col s2 pd-0" href="<?php echo $allData[0]->S_link; ?>"><span class="fa fa-skype"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ Overview Section -->


		<!-- Skill Section -->
		<section id="skill-section" class="skill-section">
			<div class="container">
				<div class="row">
					<div class="section-title">
						Skills
					</div>
					<div class="col s12 section-content skill-wrapper w-block z-depth-1 shadow-change pd-50">
                        <div class="col l6 m6 s12 skill-desc pdl-0">

                        <?php
                            $cntr = 0;
                            foreach ($allData2 as $myskill) {
                                $cntr ++;
                        ?>
                            <div class="progress-bar-wrapper">
                                <p class="progress-text"><?php echo $myskill->skill_name; ?>
                                    <span><?php echo $myskill->skill_level; ?>%</span>
                                </p>
                                <div class="progress-bar">
                                    <span data-percent="<?php echo $myskill->skill_level; ?>"></span>
                                </div>
                            </div>
                        <?php
                                if($cntr == 6) {
                        ?>
                        </div>

                        <div class="col l6 m6 s12 skill-data pdr-0">
                        <?php
                                }
                            }
                        ?>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ Skill Section -->

		<!-- Education Section -->
		<section id="education-section" class="education-section">
			<div class="container">
				<div class="row">
					<div class="section-title">
						Education
					</div>
					<div class="col s12 section-content pd-0">
						<ul class="timeline">
                            <?php
                                $cntr = 0;
                                foreach ($allData4 as $myedu) {
                            ?>

						    <li <?php echo $cntr ? 'class="timeline-inverted"' : ''; ?>>
						        <div class="timeline-badge">
						          <a><i class="fa fa-circle <?php echo $cntr ? 'invert' : ''; ?>"></i></a>
						        </div>
						        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
									<div class="timeline-title">
                                        <?php echo $myedu->degree_name; ?>
									</div>
									<div class="timeline-tag">
                                        <?php echo $myedu->institute_name; ?>
									</div>
									<div class="timeline-time">2014-2017</div>
						        </div>
						    </li>

                            <?php
                                    $cntr = $cntr ? 0 : 1;
                                }
                            ?>
						    <li class="clearfix no-float"></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!--/ Education Section -->

		<!-- Experience Section -->
		<section id="experience-section" class="experience-section">
			<div class="container">
				<div class="row">
					<div class="section-title">
						Experience
					</div>
					<div class="col s12 section-content pd-0">
						<ul class="timeline">

                            <?php
                            $cntr = 0;
                            foreach ($allData5 as $myexperience) {
                            ?>

						    <li <?php echo $cntr ? 'class="timeline-inverted"' : ''; ?>>
						        <div class="timeline-badge">
						          <a><i class="fa fa-circle <?php echo $cntr ? 'invert' : ''; ?>"></i></a>
						        </div>
						        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
									<div class="timeline-title">
                                        <?php echo $myexperience->position; ?>
									</div>
									<div class="timeline-tag">
                                        <?php echo $myexperience->company; ?>
									</div>
									<div class="timeline-desc">
										<p>
                                            <?php echo $myexperience->responsibility; ?>
										</p>
									</div>
									<div class="timeline-time">2018</div>
						        </div>
						    </li>

                                <?php
                                $cntr = $cntr ? 0 : 1;
                            }
                            ?>


						    <li class="clearfix no-float"></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!--/ Experience Section -->


		<!-- Portfolio Section -->


		<section id="portfolio-section" class="portfolio-section">
			<div class="container">
				<div class="row">
					<div class="section-title">
						Portfolio
					</div>
					<div class="col s12 section-content pd-0">

						<ul class="filter">
							<li><a class="active" href="#" data-filter="*">All</a></li>

                            <?php

                              foreach ($allData3 as $portfolio){
                               ?>
							<li><a href="#" data-filter=".<?php echo $portfolio->project_type; ?>"><?php echo $portfolio->project_type; ?> Project </a></li>
                            <?php

						    	}
                            ?>
						</ul>
						
						<ul class="portfolio-items">

                            <?php

                            foreach ($allData3 as $portfolio){
                            ?>

							<li id="portfolio-1" class="<?php echo $portfolio->project_type; ?> branding portfolio-content z-depth-1 shadow-change">
								<figure>
									<img src="../admin/portfolio/portfolioUploads/<?php echo $portfolio->image; ?>" alt="image">
									<figcaption>
										<div class="portfolio-intro">
											<div class="portfolio-intro-link">
												<a href="<?php echo $portfolio->link; ?>" target="_blank"><span><i class="fa fa-link"></i></span></a>
											</div>
											<div class="portfolio-intro-title">
                                                <?php echo $portfolio->project_title; ?>
											</div>
										</div>
									</figcaption>
								</figure>
							</li>

                                <?php

                            }

                            ?>



						</ul>
													
					</div>
				</div>
			</div>
		</section>
		<!--/ Portfolio Section -->



<section id="contact-section" class="contact-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Contact
            </div>
            <div class="col l5 m5 s12 contact-data pdl-0">
                <div class="col s12 w-block z-depth-1 shadow-change pd-0">
                    <div class="col s12 pdt-30 pdr-30 pdb-10 pdl-30">
                        <div class="arrow-box">Useful Info</div>
                        <div class="c-info">
                            <span class="fa fa-phone"></span>
                            <span>+88<?php echo $allData[0]->mobile; ?></span>
                        </div>
                        <div class="c-info">
                            <span class="fa fa-road"></span>
                            <span><?php echo $allData[0]->address; ?></span>
                        </div>
                        <div class="c-info">
                            <span class="fa fa-envelope"></span>
                            <span><?php echo $allData[0]->email; ?></span>
                        </div>

                    </div>
                    <div class="col s12 g-map-wrapper pd-0">
                        <div id="g-map" data-latitude="51.5255069" data-longitude="-0.0836207"></div>
                    </div>
                    <div class="col s12 contact-map-btn z-depth-1 shadow-change waves-effect waves-light pd-0">
                        <span class="fa fa-map-marker"></span>View Map
                    </div>
                </div>
            </div>
            <div class="col l7 m7 s12 contact-form pdr-0">
                <div class="col s12 w-block z-depth-1 shadow-change pdt-10 pdr-30 pdb-30 pdl-30">
                    <form  class="c-form" action="storeSMS.php" method="post">
                        <fieldset>

                            <div class="input-field">
                                <input id="name" type="text" name="name" class="validate">
                                <label for="name">Name</label>
                            </div>

                            <div class="input-field">
                                <input id="email" type="email" name="email" class="validate">
                                <label for="email">Email</label>
                            </div>

                            <div class="input-field">
                                <input id="subject" type="text" name="subject" class="validate">
                                <label for="subject">Subject</label>
                            </div>

                            <div class="input-field">
                                <textarea id="message" name="message" class="materialize-textarea validate"></textarea>
                                <label for="message">Message</label>
                            </div>
                            <div>
                                <button class="btn waves-effect waves-light" type="submit" name="button">Send Message</button>

                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include 'includes/footer.php'; ?>
		

		
