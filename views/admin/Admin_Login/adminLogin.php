<?php
require_once "../../../vendor/autoload.php";
if (!isset($_SESSION)) session_start();
//$_SESSION['message']="incorrect username or password";
use App\Message\Message;

$msg =Message::message();
echo $msg;
?>


<!DOCTYPE html>
<html>

<!-- Mirrored from preview.uideck.com/items/inspire-1.1/dark/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Aug 2018 08:14:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Inspire - Admin and Dashboard Template</title>

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/fonts/line-icons.css">

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/css/main.css">

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/css/responsive.css">
</head>
<body class="bg-black">
<div class="wrapper-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-12 col-xs-12">
                <div class="card bg-dark">
                    <div class="card-header border-bottom text-center">
                        <h4 class="card-title text-white">Sign In</h4>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal m-t-20" action="../authentication.php" method="post">
                            <div class="form-group">
                                <input class="form-control" type="text" name="name" required="" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" name="password" required="" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label text-white" for="customCheck1">Remember me</label>
                                </div>
                            </div>
                            <div class="form-group text-center m-t-20">
                                <button class="btn btn-inverse-primary btn-block" type="submit" name="submit">Log In</button>
                            </div>
                           <!-- <div class="form-group">
                                <div class="float-right">
                                    <a href="forgot-password.html" class="text-white"><i class="lni-lock"></i> Forgot your password?</a>
                                </div>
                                <div class="float-left">
                                    <a href="sign-up.html" class="text-white"><i class="lni-user"></i> Create an account</a>
                                </div>
                            </div>-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="preloader">
    <div class="loader" id="loader-1"></div>
</div>


<script src="../../../resource/admin/assets/js/jquery-min.js"></script>
<script src="../../../resource/admin/assets/js/popper.min.js"></script>
<script src="../../../resource/admin/assets/js/bootstrap.min.js"></script>
<script src="../../../resource/admin/assets/js/jquery.app.js"></script>
<script src="../../../resource/admin/assets/js/main.js"></script>
</body>

<!-- Mirrored from preview.uideck.com/items/inspire-1.1/dark/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Aug 2018 08:14:59 GMT -->
</html>