<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/SkillAdmin/SkillAdmin.php";

use App\Utility\Utility;

$objEmployee =  new App\SkillAdmin\SkillAdmin();

$objEmployee->setData($_POST);
$objEmployee->update();
Utility::redirect('view-skill.php');