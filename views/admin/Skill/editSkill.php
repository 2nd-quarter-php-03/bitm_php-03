<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once "../../../src/SkillAdmin/SkillAdmin.php";
require_once ("../../../src/Message/Message.php");

use App\SkillAdmin\SkillAdmin;

$viewSingleProduct = new SkillAdmin();
$singleData = $viewSingleProduct->view($_GET['id']);
echo json_encode($singleData);
//$msg = Message::message();