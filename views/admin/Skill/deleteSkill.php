<?php

$path = $_SERVER ['HTTP_REFERER'];
require_once "../../../vendor/autoload.php";
require_once "../../../src/SkillAdmin/SkillAdmin.php";

use App\Utility\Utility;

$object = new App\SkillAdmin\SkillAdmin();
$object->setData($_GET);
$object->delete();

Utility::redirect($path);