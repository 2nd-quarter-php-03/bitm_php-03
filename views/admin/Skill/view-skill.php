
<?php
session_start();
use App\Message\Message;
require_once ("../../../vendor/autoload.php");
require_once ("../../../src/Message/Message.php");
require_once "../../../src/SkillAdmin/SkillAdmin.php";

$msg = Message::message();

use App\SkillAdmin\SkillAdmin;

$object = new SkillAdmin();
$allData = $object->index();

?>

<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>



<div class="page-container">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8 col-md-12 col-xs-12">
                    <div class="card bg-dark">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="card bg-dark">
                                <div class="card-header border-bottom" style="text-align: center">
                                    <h4 class="card-title text-white" style="text-align: center"> Skill Setup Form</h4>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="dtc">
                                        <thead>
                                        <tr>
                                            <th style='text-align: center;alignment: center'>Serial</th>
                                            <th style='text-align: center;alignment: center'>Skill Name</th>
                                            <th style='text-align: center;alignment: center'>Skill Level</th>
                                            <th style='text-align: center;alignment: center'>Update</th>
                                        </tr>
                                        </thead>
                                        <?php

                                        $serial =1;

                                        foreach ($allData as $record) {

                                            echo "

                              <tbody>

                                <tr>
                                
                                  <td style='text-align: center;alignment: center'>$serial</td> 
                                  <td style='text-align: center;alignment: center'>$record->skill_name</td>
                                  <td style='text-align: center;alignment: center'>$record->skill_level</td>
                                                        

                                 <td>
                                        <button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#ordine' onClick='viewone($record->id)' data-keyboard='false' data-backdrop='static'>Edit<i class='fa fa-pencil'></i>
                                        </button>
                                        <a type='button' href='deleteSkill.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-danger btn-xs'>Delete<i class='fa fa-trash-o'></i></a>
                                    </td>


                                </tr>





                                </tbody>

                                     ";
                                      $serial++;
                                        }?>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include '../include/footer.php';?>


<script type="text/javascript">

    function confirm_delete(){

        return confirm("Are You Sure?");

    }
    function viewone(upid)
    {
        $.ajax({
            type: 'post',
            url: 'editSkill.php?id='+upid,
            success: function(data)
            {
                var obj = JSON.parse(data);
                $('#ordine input[name="id"]').val(obj.id);
                $('#ordine input[name="skill_name"]').val(obj.skill_name);
                $('#ordine input[name="skill_level"]').val(obj.skill_level);

            }
        });
    }


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )


    // <!--search Option-->


    function filtab() {
        var input = document.getElementById("sfdt");
        var filter = input.value.toUpperCase();
        var table = document.getElementById("dtc");
        var r=1, f=0;
        while(row=table.rows[r++])
        {
            var c=0, k=0;
            while(cell=row.cells[c++])
            {
                if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    k++;
                }
            }
            if(k>0){
                row.style.display = "";
                f=1;
            } else {
                row.style.display = "none";
            }
        }

    }

    function remimg() {
        $(".splt").remove();
    }

</script>


<div id="ordine" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content bg-dark">
            <div class="modal-header">
                <h4 class="modal-title">Update Skill table</h4>
                <button type="button" class="close" data-dismiss="modal" onclick="remimg()">×</button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">

                        <form class="forms-sample" action="skillUpdate.php" method="post" enctype="multipart/form-data">

                            <?php
                            echo " 
                                    <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                               ";?>

                            <input type="hidden" name="id" value="" />

                            <div class="form-group row">
                                <label for="skill_name" class="col-sm-3 col-form-label text-white">Skill Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="skill_name" name="skill_name" placeholder="Skill Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="skill_level" class="col-sm-3 col-form-label text-white">Skill level</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="skill_level" name="skill_level" placeholder="Skill level">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-common mr-3">Update</button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>








<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>