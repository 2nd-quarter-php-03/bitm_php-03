<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/SkillAdmin/SkillAdmin.php";

use App\Utility\Utility;

$objProfileAdmin =  new App\SkillAdmin\SkillAdmin();

$objProfileAdmin->setData($_POST);
$objProfileAdmin->store();

Utility::redirect('add-skill.php');