<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once ("../../../src/Message/Message.php");
require_once ("../../../src/Message/Message.php");

use App\Message\Message;
$msg = Message::message();
?>
<?php include '../include/head.php';?>
<?php include '../include/sidebar.php';?>


<div class="page-container">
    <div class="main-content">
        <div class="container-fluid">
            <div class="breadcrumb-wrapper row">
                <div class="col-12 col-lg-3 col-md-6">
                    <h4 class="page-title">Dashboard</h4>
                </div>
                <div class="col-12 col-lg-9 col-md-6">
                    <ol class="breadcrumb float-right">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">/ Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
        <?php
        echo " <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>  ";?>

        <div class="container-fluid">
            <div class="card-group">
                <div class="card bg-dark">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <div class="icon"><i class="lni-display"></i></div>
                                        <p class="text-white">New Clients</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h2 class="counter text-primary">234</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card bg-dark">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <div class="icon"><i class="lni-pencil-alt"></i></div>
                                        <p class="text-white">Total Projects</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h2 class="counter text-success">1,390</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card bg-dark">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <div class="icon"><i class="lni-empty-file"></i></div>
                                        <p class="text-white">Pending Invoices</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h2 class="counter text-info">5,723</h2>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card bg-dark">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <div class="icon"><i class="lni-cart"></i></div>
                                        <p class="text-white">All Projects</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h2 class="counter text-purple">2,396</h2>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-purple" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php include '../include/footer.php';?>
    <script>
        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>