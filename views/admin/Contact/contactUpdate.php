<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/ContactAdmin/ContactAdmin.php";

use App\Utility\Utility;
use App\ContactAdmin\ContactAdmin;
$fileName = time().$_FILES['image']['name'];

$source = $_FILES['image']['tmp_name'];
$destination = "../ContactImages/".$fileName;
move_uploaded_file($source,$destination);


$object = new ContactAdmin();


$_POST['image'] = $fileName;

$object->setData($_POST);

$object->update();

Utility::redirect('view-contact.php');
