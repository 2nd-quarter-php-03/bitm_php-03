<?php

$path = $_SERVER ['HTTP_REFERER'];
require_once "../../../vendor/autoload.php";
require_once "../../../src/ContactAdmin/ContactAdmin.php";

use App\ContactAdmin\ContactAdmin;
use App\Utility\Utility;

$object = new ContactAdmin();
$object->setData($_GET);
$object->delete();
Utility::redirect($path);