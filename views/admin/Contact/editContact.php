

<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>

<?php

    require_once ("../../../vendor/autoload.php");
    require_once ("../../../src/Message/Message.php");
?>


<?php

    require_once "../../../src/ContactAdmin/ContactAdmin.php";

    use App\ContactAdmin\ContactAdmin;
    use App\Utility\Utility;
    use App\Message;


    $object = new ContactAdmin();
    $object->setData($_GET);
    $singleData = $object->view();

?>


<div class="page-container">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-6 col-md-12 col-xs-12">
                    <div class="card bg-dark">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="card bg-dark">
                                <div class="card-header border-bottom">
                                    <h4 class="card-title text-white" style="text-align: center"> Contact Update Form</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-description text-white"></p>
                                    <form class="forms-sample" action="contactUpdate.php" method="post" enctype="multipart/form-data">


                                        <div class="form-group row">
                                            <label for="mobile" class="col-sm-3 col-form-label text-white">Mobile</label>
                                            <div class="col-sm-9">
                                                <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
                                                <input type="text" value="<?php echo $singleData->mobile?>" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-sm-3 col-form-label text-white">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $singleData->email?>" class="form-control" id="email" name="email" placeholder="http//">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="address" class="col-sm-3 col-form-label text-white">Address</label>
                                            <div class="col-sm-9">
                                                <textarea type="text" class="form-control" id="address" name="address" placeholder="Address"><?php echo $singleData->address?></textarea>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="address" class="col-sm-3 col-form-label text-white">Address</label>
                                            <div class="col-sm-9">
                                                <textarea type="text" class="form-control" id="address" name="address" placeholder="Address"><?php echo $singleData->address?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="address" class="col-sm-3 col-form-label text-white">In</label>
                                            <div class="col-sm-9">
                                                <input type="time" class="form-control" id="in_time" name="in_time" value="<?php echo $singleData->in_time?>"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="address" class="col-sm-3 col-form-label text-white">Out</label>
                                            <div class="col-sm-9">
                                                <input type="time" class="form-control" id="out_time" name="out_time" value="<?php echo $singleData->out_time?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-sm-3 col-form-label text-white">Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="link" name="link" placeholder="Google Image Link" value="<?php echo $singleData->link?>">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="image" class="col-sm-3 col-form-label text-white">Image</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" name="image" id="image">
                                                <img src="../ContactImages/<?php echo $singleData->image ?>" alt=""  height="80" width="150">
                                            </div>
                                        </div>
                                        <button type="submit" value="Update" class="btn btn-common mr-3">Update</button>
                                        <!--<button class="btn btn-light">Cancel</button>-->



                                        <!--  <div class="form-group row">
                                              <div class="col-sm-9">

                                              </div>
                                          </div>-->

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

<?php include '../include/footer.php';?>