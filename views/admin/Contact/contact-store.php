<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/ContactAdmin/ContactAdmin.php";

use App\Utility\Utility;

$fileName = time().$_FILES['image']['name'];

$source = $_FILES['image']['tmp_name'];
$destination = "../ContactImages/".$fileName;
move_uploaded_file($source,$destination);


$objTrainingAdmin =  new App\ContactAdmin\ContactAdmin();

$_POST['image'] = $fileName;

$objTrainingAdmin->setData($_POST);

$objTrainingAdmin->store();

Utility::redirect('add-contact.php');