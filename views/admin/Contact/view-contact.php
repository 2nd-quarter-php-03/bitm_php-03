<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once "../../../src/ContactAdmin/ContactAdmin.php";
require_once ("../../../src/Message/Message.php");

use App\Message\Message;


use App\ContactAdmin\ContactAdmin;

$object = new ContactAdmin();


$allData = $object->index();

$msg = Message::message();
?>
<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>




<div class="page-container">
    <div class="main-content">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="card bg-dark">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="card bg-dark">
                            <div class="card-header border-bottom" style="text-align: center">
                                <h4 class="card-title text-white" style="text-align: center;" > View Setup Form</h4>
                            </div>
                            <div class="card-body" style="text-align: center">
                                <p class="card-description text-white"></p>

                                <form class="forms-sample" action="contact-store.php" method="post" style="text-align: center">
                                    <div class="form-group row" style="text-align: center">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10">
                                            <table style="box-shadow: 0 0  25px #fdfff4;alignment: center;text-align: center">

                                                <thead class="table-bordered table-hover" style="text-align: center;alignment: center">
                                                <tr>
                                                    <!--<th style="text-align:center ;font-weight:bolder;font-size: large; color: #ffffff">Check All <input type='checkbox'  class='checkbox' name='multiple[]' value='$record->id'></th>-->
                                                    <th style="font-weight: bolder;font-size: small;text-align: center;color: #ffffff">Serial</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff">Mobile Number</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff">Email Address</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center;color: #ffffff ;width: 10%;height: auto">Address</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center;color: #ffffff ;width: 10%;height: auto">In</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center;color: #ffffff ;width: 10%;height: auto">Out</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center;color: #ffffff ;width: 10%;height: auto">Link</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center;color: #ffffff ;width: 10%;height: auto">Image</th>

                                                    <th style="text-align: center;font-weight: bolder;font-size: large;color: #ffffff">Action</th>
                                                </tr>
                                                </thead>
                                                <?php
                                                $serial =1;
                                                foreach ($allData as $record)
                                                {
                                                    echo "                      
                                                <tbody class='table-striped table-bordered' >
                                                   <tr>
                                                        
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$serial</td>
                                                        
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->mobile</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->email</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff;max-width: 100px''>$record->address</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff;max-width: 100px''>$record->in_time</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff;max-width: 100px''>$record->out_time</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff;max-width: 200px''>$record->link</td>
                                                        <td style='text-align:center;color: #ffffff'><img style='padding-top: 10px' src='../ContactImages/$record->image' height='70' width='50' alt=''></td>
                                                        <td>
                                               
                                                            <a href='editContact.php?id=$record->id' class='btn btn-inverse-primary' style='margin: 2px;border-radius: 5px;margin-right: 3px;width: 80px'><span class='glyphicon glyphicon-pencil'></span> Edit </a>
                                                            
                                                            <a href='deleteContact.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-inverse-danger' style='margin: 2px;border-radius: 5px;width: 80px;margin-right: 3px'><span class='glyphicon glyphicon-remove'></span> Delete</a>
                                                           
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                 
                                                    ";
                                                    $serial++;
                                                }
                                                ?>
                                            </table>

                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function confirm_delete(){

        return confirm("Are You Sure?");

    }
</script>
<script>

    function checkEmptySelection(){

        emptySelection =true;

        $('.checkbox').each(function(){
            if(this.checked)   emptySelection = false;
        });

        return emptySelection;
    }
</script>


<script>

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });
</script>
<?php include '../include/footer.php';?>