<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once "../../../src/ProfileAdmin/ProfileAdmin.php";
require_once ("../../../src/Message/Message.php");

use App\ProfileAdmin\ProfileAdmin;

$viewSingleProduct = new ProfileAdmin();
$singleData = $viewSingleProduct->view($_GET['id']);
echo json_encode($singleData);
//$msg = Message::message();