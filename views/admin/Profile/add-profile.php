
<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once ("../../../src/Message/Message.php");

use App\Message\Message;
$msg = Message::message();
?>

<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>



<div class="page-container">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-6 col-md-12 col-xs-12">
                    <div class="card bg-dark">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="card bg-dark">
                                <div class="card-header border-bottom" style="text-align: center">
                                    <h4 class="card-title text-white" style="text-align: center"> Contact Setup Form</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-description text-white"></p>


                                    <form class="forms-sample" action="profile-store.php" method="post" enctype="multipart/form-data">
                                        <?php
                                        echo " 
                                         <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                                      ";?>

                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label text-white">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="title" class="col-sm-3 col-form-label text-white">Title/Position</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="title" name="title" placeholder="Title/Position">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="age" class="col-sm-3 col-form-label text-white">Age</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="age" name="age" placeholder="Age">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="address" class="col-sm-3 col-form-label text-white">Address</label>
                                            <div class="col-sm-9">
                                                <textarea type="text" class="form-control" id="address" name="address" placeholder="Address"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-sm-3 col-form-label text-white">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="mobile" class="col-sm-3 col-form-label text-white">Mobile</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label for="L_link" class="col-sm-3 col-form-label text-white">LinkedIN Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="L_link" name="L_link" placeholder="LinkedIN Link">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="T_link" class="col-sm-3 col-form-label text-white">Twitter Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="T_link" name="T_link" placeholder="Twitter Link">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="G_link" class="col-sm-3 col-form-label text-white">GitHub Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="G_link" name="G_link" placeholder="GitHub Link">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="F_link" class="col-sm-3 col-form-label text-white">FaceBook Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="F_link" name="F_link" placeholder="FaceBook Link">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="S_link" class="col-sm-3 col-form-label text-white">SKYPE Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="S_link" name="S_link" placeholder="SKYPE Link">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="p_image" class="col-sm-3 col-form-label text-white">Profile Upload</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" name="p_image" id="p_image">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="r_image" class="col-sm-3 col-form-label text-white">Resume Upload</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" name="r_image" id="r_image">
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-common mr-3">Submit</button>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include '../include/footer.php';?>
<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>