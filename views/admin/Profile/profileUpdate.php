<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/ProfileAdmin/ProfileAdmin.php";

use App\Utility\Utility;

$fileName = $_FILES['p_image']['name'];
$source1 = $_FILES['p_image']['tmp_name'];
$dest = "ProfileFiles/".$fileName;
move_uploaded_file($source1,$dest);


$file = $_FILES['r_image']['name'];
$source = $_FILES['r_image']['tmp_name'];
$destination = "ProfileFiles/".$file;
move_uploaded_file($source,$destination);


$objEmployee =  new App\ProfileAdmin\ProfileAdmin();

$_POST['p_image'] = $fileName;
$_POST['r_image'] = $file;


//Utility::dd($_POST);

$objEmployee->setData($_POST);
$objEmployee->update();
Utility::redirect('view-profile.php');