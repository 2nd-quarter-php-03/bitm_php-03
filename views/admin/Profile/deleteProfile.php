<?php

$path = $_SERVER ['HTTP_REFERER'];
require_once "../../../vendor/autoload.php";
require_once "../../../src/ProfileAdmin/ProfileAdmin.php";

use App\Utility\Utility;

$object = new App\ProfileAdmin\ProfileAdmin();
$object->setData($_GET);
$object->delete();

Utility::redirect($path);