
<?php
session_start();
use App\Message\Message;
require_once ("../../../vendor/autoload.php");
require_once ("../../../src/Message/Message.php");
require_once "../../../src/ProfileAdmin/ProfileAdmin.php";

$msg = Message::message();

use App\ProfileAdmin\ProfileAdmin;

$object = new ProfileAdmin();
$allData = $object->index();

?>

<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>



<div class="page-container">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="card bg-dark">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="card bg-dark">
                                <div class="card-header border-bottom" style="text-align: center">
                                    <h4 class="card-title text-white" style="text-align: center"> Profile Setup Form</h4>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="dtc">
                                        <thead>
                                        <tr>
                                            <th style='text-align: center;alignment: center'>Name</th>
                                            <th style='text-align: center;alignment: center'>Title/Position</th>
                                            <th style='text-align: center;alignment: center'>Age</th>
                                            <th style='text-align: center;alignment: center'>Address</th>
                                            <th style='text-align: center;alignment: center'>Email</th>
                                            <th style='text-align: center;alignment: center'>Mobile</th>
                                            <th style='text-align: center;alignment: center'>LinkedIn</th>
                                            <th style='text-align: center;alignment: center'>Twitter Link</th>
                                            <th style='text-align: center;alignment: center'>GitHub</th>
                                            <th style='text-align: center;alignment: center'>FaceBook Link</th>
                                            <th style='text-align: center;alignment: center'>SKYPE Link</th>
                                            <th style='text-align: center;alignment: center'>Profile Upload</th>
                                            <th style='text-align: center;alignment: center'>Resume Upload</th>
                                            <th style='text-align: center;alignment: center'>Update</th>
                                        </tr>
                                        </thead>
                                        <?php

                                        foreach ($allData as $record) {

                                        echo "

                              <tbody>


                                <tr>

                                
                                  <td style='text-align: center;alignment: center'>$record->name</td>
                                  <td style='text-align: center;alignment: center'>$record->title</td>
                                  <td style='text-align: center;alignment: center'>$record->age</td>
                                  <td style='text-align: center;alignment: center'>$record->address</td>
                                  <td style='text-align: center;alignment: center'>$record->email</td>
                                  <td style='text-align: center;alignment: center'>$record->mobile</td>
                                  <td style='text-align: center;alignment: center'>$record->L_link</td>
                                  <td style='text-align: center;alignment: center'>$record->T_link</td>
                                  <td style='text-align: center;alignment: center'>$record->G_link</td>
                                  <td style='text-align: center;alignment: center'>$record->F_link</td>
                                  <td style='text-align: center;alignment: center'>$record->S_link</td>
                                  <td style='text-align: center;alignment: center'><img style='padding-top: 10px' src='ProfileFiles/$record->p_image' height='70' width='50' alt=''></td>
                                  <td style='text-align: center;alignment: center'><a href='ProfileFiles/$record->r_image' target='_blank'>Resume</a></td>
                                

                                 <td>
                                        <button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#ordine' onClick='viewone($record->id)' data-keyboard='false' data-backdrop='static'>Edit<i class='fa fa-pencil'></i>
                                        </button>
                                        <a type='button' href='deleteProfile.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-danger btn-xs'>Delete<i class='fa fa-trash-o'></i></a>
                                    </td>


                                </tr>





                                </tbody>

                                     ";
                                        }?>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include '../include/footer.php';?>


<script type="text/javascript">

    function confirm_delete(){

        return confirm("Are You Sure?");

    }
    function viewone(upid)
    {
        $.ajax({
            type: 'get',
            url: 'editProfile.php?id='+upid,
            success: function(data)
            {
                var obj = JSON.parse(data);
                $('#ordine input[name="id"]').val(obj.id);
                $('#ordine input[name="name"]').val(obj.name);
                $('#ordine input[name="title"]').val(obj.title);
                $('#ordine input[name="age"]').val(obj.age);
                $('#ordine textarea[name="address"]').text(obj.address);
                $('#ordine input[name="email"]').val(obj.email);
                $('#ordine input[name="mobile"]').val(obj.mobile);
                $('#ordine input[name="L_link"]').val(obj.L_link);
                $('#ordine input[name="T_link"]').val(obj.T_link);
                $('#ordine input[name="G_link"]').val(obj.G_link);
                $('#ordine input[name="F_link"]').val(obj.F_link);
                $('#ordine input[name="S_link"]').val(obj.S_link);

                $('#ordine #pimg').attr('src', 'ProfileFiles/'+obj.p_image);
                $('#ordine img.oimg1').attr('src', 'ProfileFiles/'+obj.r_image);

                //$(".oldimg").append("<img class='splt' src='ProfileFiles/"+ splitimg[i] +"' width='100px' height='auto' style='margin: 5px;' />");


            }
        });
    }

    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )


    // <!--search Option-->


    function filtab() {
        var input = document.getElementById("sfdt");
        var filter = input.value.toUpperCase();
        var table = document.getElementById("dtc");
        var r=1, f=0;
        while(row=table.rows[r++])
        {
            var c=0, k=0;
            while(cell=row.cells[c++])
            {
                if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    k++;
                }
            }
            if(k>0){
                row.style.display = "";
                f=1;
            } else {
                row.style.display = "none";
            }
        }

    }

    function remimg() {
        $(".splt").remove();
    }

</script>


<div id="ordine" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content bg-dark">
            <div class="modal-header">
                <h4 class="modal-title">Update Profile table</h4>
                <button type="button" class="close" data-dismiss="modal" onclick="remimg()">×</button>
              </div>
         <div class="modal-body">

        <div class="row">
             <div class="col-lg-12 col-md-12 col-xs-12">

                        <form class="forms-sample" action="profileUpdate.php" method="post" enctype="multipart/form-data">

                            <?php
                             echo " 
                                    <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                               ";?>

                            <input type="hidden" name="id" value="" />

                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label text-white">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="title" class="col-sm-3 col-form-label text-white">Title/Position</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title/Position">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="age" class="col-sm-3 col-form-label text-white">Age</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="age" name="age" placeholder="Age">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="address" class="col-sm-3 col-form-label text-white">Address</label>
                                <div class="col-sm-9">
                                    <textarea type="text" class="form-control" id="address" name="address" placeholder="Address"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label text-white">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="mobile" class="col-sm-3 col-form-label text-white">Mobile</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="L_link" class="col-sm-3 col-form-label text-white">LinkedIN Link</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="L_link" name="L_link" placeholder="LinkedIN Link">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="T_link" class="col-sm-3 col-form-label text-white">Twitter Link</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="T_link" name="T_link" placeholder="Twitter Link">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="G_link" class="col-sm-3 col-form-label text-white">GitHub Link</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="G_link" name="G_link" placeholder="GitHub Link">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="F_link" class="col-sm-3 col-form-label text-white">FaceBook Link</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="F_link" name="F_link" placeholder="FaceBook Link">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="S_link" class="col-sm-3 col-form-label text-white">SKYPE Link</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="S_link" name="S_link" placeholder="SKYPE Link">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="p_image" class="col-sm-3 col-form-label text-white">Profile Upload</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control" name="p_image" id="p_image">
                                    <img id="pimg" height="80px" width="auto">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="r_image" class="col-sm-3 col-form-label text-white">Resume Upload</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control" name="r_image" id="r_image">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-common mr-3">Update</button>

                        </form>

    </div>
</div>
 </div>
 </div>
</div>
</div>








<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>