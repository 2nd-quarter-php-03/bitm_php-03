<?php

$path = $_SERVER ['HTTP_REFERER'];
require_once "../../../vendor/autoload.php";
require_once "../../../src/ExperienceAdmin/ExperienceAdmin.php";

use App\Utility\Utility;

$objExperienceAdmin = new App\ExperienceAdmin\ExperienceAdmin();
$objExperienceAdmin->setData($_GET);
$objExperienceAdmin->delete();

Utility::redirect($path);