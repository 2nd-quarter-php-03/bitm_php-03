<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/ExperienceAdmin/ExperienceAdmin.php";

use App\Utility\Utility;

$objExperienceAdmin =  new App\ExperienceAdmin\ExperienceAdmin();

$objExperienceAdmin->setData($_POST);
$objExperienceAdmin->store();

Utility::redirect('add-experience.php');