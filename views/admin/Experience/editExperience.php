<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once "../../../src/ExperienceAdmin/ExperienceAdmin.php";
require_once ("../../../src/Message/Message.php");

use App\ExperienceAdmin\ExperienceAdmin;

$viewExperienceAdmin= new ExperienceAdmin();
$singleData = $viewExperienceAdmin->view($_GET['id']);
echo json_encode($singleData);
//$msg = Message::message();