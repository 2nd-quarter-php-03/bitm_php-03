<?php

session_start();

use App\Message\Message;

require_once ("../../../src/Message/Message.php");
require_once ("../../../vendor/autoload.php");
require_once "../../../src/Feedback/Feedback.php";

$object = new \App\Feedback\Feedback();
$allData = $object->index();
$msg = Message::message();
?>
<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>


<div class="page-container">
    <div class="main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="card bg-dark">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="card bg-dark">
                        <div class="card-header border-bottom" style="text-align: center">
                            <h4 class="card-title text-white" style="text-align: center;" > View Messages </h4>
                        </div>
                        <div class="card-body" style="text-align: center">
                            <p class="card-description text-white"></p>

                            <form class="forms-sample" action="" method="post" style="text-align: center">

                                <div class="form-group row" style="text-align: center">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <table style="box-shadow: 0 0  25px #fdfff4;alignment: center;text-align: center">

                                            <thead class="table-bordered table-hover" style="text-align: center;alignment: center">
                                            <tr>
                                                <!--<th style="text-align:center ;font-weight:bolder;font-size: large; color: #ffffff">Check All <input type='checkbox'  class='checkbox' name='multiple[]' value='$record->id'></th>-->
                                                <th style="font-weight: bolder;font-size: small;text-align: center;color: #ffffff;padding: 20px">Serial</th>
                                                <th style="font-weight: bolder;font-size: medium;text-align: center;color: #ffffff;padding: 20px">Date</th>
                                                <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff;padding: 20px">Name</th>
                                                <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff;padding: 20px">Email</th>
                                                <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff;max-width: 10%;padding: 20px">Subject</th>
                                                <th style="text-align: center;font-weight: bolder;font-size: large;color: #ffffff;padding: 20px">Message</th>
                                                <th style="text-align: center;font-weight: bolder;font-size: large;color: #ffffff;padding: 20px">Action</th>
                                            </tr>
                                            </thead>
                                            <?php
                                            $serial =1;
                                            foreach ($allData as $record)
                                            {
                                                echo "                      
                                                <tbody class='table-striped table-bordered'>
                                                   <tr>
                                                        
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$serial</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->date</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->name</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->email</td>                                                     
                                                        <td style='text-align:center;font-size: small;color: #ffffff;max-width: 100px'>$record->subject</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff;max-width: 500px'>$record->message</td>
                                                        <td>
                                               
                                                            <a href='deleteMessage.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-inverse-warning' style='border-radius: 5px;width: 80px;margin: 5px'><span class='glyphicon glyphicon-remove'></span> Delete</a>
                                                           
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                 
                                                    ";
                                                $serial++;
                                            }
                                            ?>
                                        </table>

                                    </div>
                                    <div class="col-md-1"></div>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
</div>
<script>

    function confirm_delete(){

        return confirm("Are You Sure?");

    }
</script>

<?php include '../include/footer.php';?>
<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
