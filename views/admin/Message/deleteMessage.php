<?php

$path = $_SERVER ['HTTP_REFERER'];
require_once "../../../vendor/autoload.php";
require_once "../../../src/Training/TrainingAdmin.php";

use App\Feedback\Feedback;
use App\Utility\Utility;

$object = new Feedback();
$object->setData($_GET);
$object->delete();
Utility::redirect($path);


