<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once "../../../src/EducationAdmin/EducationAdmin.php";
require_once ("../../../src/Message/Message.php");

use App\EducationAdmin\EducationAdmin;

$viewEducationAdmin = new EducationAdmin();
$singleData = $viewEducationAdmin->view($_GET['id']);
echo json_encode($singleData);
//$msg = Message::message();