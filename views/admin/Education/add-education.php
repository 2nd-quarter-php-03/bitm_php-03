
<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once ("../../../src/Message/Message.php");

use App\Message\Message;
$msg = Message::message();
?>

<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>



<div class="page-container">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-6 col-md-12 col-xs-12">
                    <div class="card bg-dark">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="card bg-dark">
                                <div class="card-header border-bottom" style="text-align: center">
                                    <h4 class="card-title text-white" style="text-align: center"> Education Setup Form</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-description text-white"></p>


                                    <form class="forms-sample" action="education-store.php" method="post" enctype="multipart/form-data">
                                        <?php
                                        echo " 
                                         <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                                      ";?>

                                        <div class="form-group row">
                                            <label for="degree_name" class="col-sm-3 col-form-label text-white">Degree/Course Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="degree_name" name="degree_name" placeholder="Degree/Course Name">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="institute_name" class="col-sm-3 col-form-label text-white">Institute Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="institute_name" name="institute_name" placeholder="Institute level">
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-common mr-3">Submit</button>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include '../include/footer.php';?>
<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>