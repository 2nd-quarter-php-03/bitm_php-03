<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/EducationAdmin/EducationAdmin.php";

use App\Utility\Utility;

$objEducationAdmin =  new App\EducationAdmin\EducationAdmin();

$objEducationAdmin->setData($_POST);
$objEducationAdmin->update();
Utility::redirect('view-education.php');