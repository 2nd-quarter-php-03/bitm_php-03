<?php

$path = $_SERVER ['HTTP_REFERER'];
require_once "../../../vendor/autoload.php";
require_once "../../../src/EducationAdmin/EducationAdmin.php";

use App\Utility\Utility;

$objEducationAdmin = new App\EducationAdmin\EducationAdmin();
$objEducationAdmin->setData($_GET);
$objEducationAdmin->delete();

Utility::redirect($path);