<div class="side-nav side-nav-expand" style="background: #06081e;">
    <div class="side-nav-inner">
        <ul class="side-nav-menu">



            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="#">
              <span class="icon-holder">
                 <i class="lni-book"></i>
              </span>
                    <span class="title">Profile</span>
                    <span class="arrow">
                  <i class="lni-chevron-right"></i>
               </span>
                </a>
                <ul class="dropdown-menu sub-down">
                    <li><a href="../Profile/add-profile.php">Add Profile</a></li>
                    <li><a href="../Profile/view-profile.php">View Profile</a></li>
                </ul>
            </li>



            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="#">
           <span class="icon-holder">
               <i class="lni-package"></i>
           </span>
                    <span class="title">Skill Setup</span>
                    <span class="arrow">
                <i class="lni-chevron-right"></i>
             </span>
                </a>
                <ul class="dropdown-menu sub-down">
                    <li>
                        <a href="../Skill/add-skill.php">Add Skill</a>
                    </li>
                    <li>
                        <a href="../Skill/view-skill.php">View Skill</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="#">
              <span class="icon-holder">
              <i class="lni-world"></i>
             </span>
                    <span class="title">Education</span>
                    <span class="arrow"><i class="lni-chevron-right"></i>
             </span>
                </a>
                <ul class="dropdown-menu sub-down">
                    <li><a href="../Education/add-education.php">Add Service</a></li>
                    <li><a href="../Education/view-education.php">View Service</a></li>
                </ul>
            </li>


            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="#">
             <span class="icon-holder">
               <i class="lni-laptop"></i>
             </span>
                    <span class="title">Experience</span>
                    <span class="arrow">
               <i class="lni-chevron-right"></i>
              </span>
                </a>
                <ul class="dropdown-menu sub-down">
                    <li><a href="../Experience/add-experience.php">Add Experience </a></li>
                    <li><a href="../Experience/view-experience.php">View Experience</a></li>
                </ul>
            </li>


            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="#">
              <span class="icon-holder">
                 <i class="lni-book"></i>
              </span>
                    <span class="title">Portfolio</span>
                    <span class="arrow">
                  <i class="lni-chevron-right"></i>
               </span>
                </a>
                <ul class="dropdown-menu sub-down">
                    <li><a href="../Portfolio/add-portfolio.php">Add Portfolio</a></li>
                    <li><a href="../Portfolio/view-portfolio.php">View Portfolio</a></li>
                </ul>
            </li>


            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="#">
              <span class="icon-holder">
                 <i class="lni-phone"></i>
              </span>
                    <span class="title">Contact</span>
                    <span class="arrow">
                  <i class="lni-chevron-right"></i>
               </span>
                </a>
                <ul class="dropdown-menu sub-down">
                    <li><a href="../Contact/add-contact.php">Contact Form</a></li>
                    <li><a href="../Contact/view-contact.php">View Contact</a></li>
                </ul>
            </li>

<!--            <li class="nav-item dropdown">-->
<!--                <a class="dropdown-toggle" href="#">-->
<!--              <span class="icon-holder">-->
<!--                 <i class="lni-slice"></i>-->
<!--              </span>-->
<!--                    <span class="title">Slider</span>-->
<!--                    <span class="arrow">-->
<!--                  <i class="lni-chevron-right"></i>-->
<!--               </span>-->
<!--                </a>-->
<!--                <ul class="dropdown-menu sub-down">-->
<!--                    <li><a href="../Slider/add-slider.php">Add Slide</a></li>-->
<!--                    <li><a href="../Slider/view-slider.php">View Slide</a></li>-->
<!--                </ul>-->
<!--            </li>-->



            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="#">
              <span class="icon-holder">
                 <i class="lni-envelope"></i>
              </span>
                    <span class="title">Message</span>
                    <span class="arrow">
                  <i class="lni-chevron-right"></i>
               </span>
                </a>
                <ul class="dropdown-menu sub-down">
                    <!--<li><a href="add-contact.php">Contact Form</a></li>-->
                    <li><a href="../Message/view-feedback.php">View Message</a></li>
                </ul>
            </li>


        </ul>
    </div>
</div>