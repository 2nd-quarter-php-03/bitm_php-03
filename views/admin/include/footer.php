<footer class="content-footer bg-dark">
    <div class="footer">
        <div class="copyright">
            <span class="text-white">Copyright © 2018 <b class="text-white">KDA IT</b></span>
            <span class="go-right">
           <a href="#" class="text-white">Term &amp; KDA IT</a>
           <a href="#" class="text-white">Privacy &amp; Policy</a>
           </span>
        </div>
    </div>
</footer>

</div>

</div>
</div>

<div id="preloader">
    <div class="loader" id="loader-1"></div>
</div>


<script src="../../../resource/admin/assets/js/jquery-min.js"></script>
<script src="../../../resource/admin/assets/js/popper.min.js"></script>
<script src="../../../resource/admin/assets/js/bootstrap.min.js"></script>
<script src="../../../resource/admin/assets/js/jquery.app.js"></script>
<script src="../../../resource/admin/assets/js/main.js"></script>

<script src="../../../resource/admin/assets/plugins/morris/morris.min.js"></script>
<script src="../../../resource/admin/assets/plugins/raphael/raphael-min.js"></script>
<script src="../../../resource/admin/assets/js/dashborad1.js"></script>

<script>

    var tgl = 1;

    $("#menu-tgl").click(function (e) {

        if(tgl) {
            tgl = 0;
            $(".side-nav").css("cssText", "background: #06081e; left: 0 !important;");
        } else {
            tgl = 1;
            $(".side-nav").css("cssText", "background: #06081e; left: -250px !important;");
        }
    });


</script>

</body>

<!-- Mirrored from preview.uideck.com/items/inspire-1.1/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Aug 2018 08:10:16 GMT -->
</html>