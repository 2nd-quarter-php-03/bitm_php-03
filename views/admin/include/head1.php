<?php
if (!isset($_SESSION))
{
    session_start();
}


if (!isset($_SESSION['username']) && empty($_SESSION['username']) && is_null($_SESSION['username'])) {

    header('location: ../');
}


    require_once '../../../vendor/autoload.php';
    require_once '../../../src/Feedback/Feedback.php';

    use App\Feedback\Feedback;

    $message = new Feedback();
    $result = $message->viewMessage();

?>

<!DOCTYPE html>
<html>

<!-- Mirrored from preview.uideck.com/items/inspire-1.1/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Aug 2018 08:09:40 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>KDA IT - Admin and Dashboard </title>

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/js/bootstrap.min.js">

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/fonts/line-icons.css">

    <link rel="stylesheet" href="../../../resource/admin/assets/plugins/morris/morris.css">

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/css/main.css">

    <link rel="stylesheet" type="text/css" href="../../../resource/admin/assets/css/responsive.css">
</head>
<body>
<div class="app header-default side-nav-dark ">
    <div class="layout">

        <div class="header navbar">
            <div class="header-container">
                <div class="nav-logo">
                    <a href="../Dashboard/dashBoard.php" style="margin-left: 15px;">
                        <b><img src="../../../resource/admin/assets/img/kdait.png" alt="" style="width: 100px;height: auto;"></b>
                    </a>
                </div>

<!--
                <ul class="nav-left">
                    <li>
                        <a class="sidenav-fold-toggler" href="javascript:void(0);">
                            <i class="lni-arrow-left-circle"></i>
                        </a>
                        <a class="sidenav-expand-toggler" href="javascript:void(0);">
                            <i class="lni-menu"></i>
                        </a>
                    </li>
                </ul>
-->

                <ul class="nav-right">
                    <!--<li class="search-box">
                        <input class="form-control" type="text" placeholder="Type to search...">
                        <i class="lni-search"></i>
                    </li>-->

                    <li class="user-profile dropdown dropdown-animated scale-left">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="lni-menu"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-md">
                            <li>
                                <ul class="list-media">
                                    <li class="list-item avatar-info">

                                        <div class="info">
                                            <span class="title text-semibold">KDA IT</span>
                                            <!--<span class="sub-title">Software Engineer</span>-->
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li role="separator" class="divider"></li>

                            <li>
                                <a href="../Message/view-feedback.php">
                                    <i class="lni-envelope"></i>
                                    <span>Inbox</span>
                                    <span class="badge badge-pill badge-primary pull-right"><?php echo  $result ?></span>
                                </a>
                            </li>

                            <li >
                                <a href="../Login/adminLogout.php">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>

