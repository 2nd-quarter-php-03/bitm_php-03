<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/Auth/Auth.php";

use App\Message\Message;
use App\Utility\Utility;
use App\Auth\Auth;
session_start();
if (isset($_SESSION['username'])) {
    header('location: ../Dashboard/dashBoard.php');
}


$Auth = new Auth();
$Auth->setData($_POST);

$check=$Auth->login();


if($check==0){
    $_SESSION['username']="";
    Message::message("Password is Wrong");
    Utility::redirect("../index.php");
}
elseif ($check==1){
    $_SESSION['username']="";
    Message::message("Your name is incorrect");
    $_SESSION['message']= "Your username is incorrect";
    Utility::redirect("../index.php");
//    header('location:excelUpload.php');
}
else{

    $_SESSION['username']=$check;
    $_SESSION['message']= "Successfully logged";
    Message::message("Successfully logged");
    Utility::redirect('../Dashboard/dashBoard.php');
}