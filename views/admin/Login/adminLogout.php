<?php
session_start();

require_once "../../../vendor/autoload.php";
if(isset($_SESSION['username']))
{
    $_SESSION['username']="";
    session_destroy();
    header("Location: ../");
}
