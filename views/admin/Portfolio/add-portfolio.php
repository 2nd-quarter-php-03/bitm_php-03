<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once ("../../../src/Message/Message.php");

use App\Message\Message;
$msg = Message::message();
?>
<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>



<div class="page-container">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-6 col-md-12 col-xs-12">
                    <div class="card bg-dark">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="card bg-dark">
                                <div class="card-header border-bottom">
                                    <h4 class="card-title text-white" style="text-align: center"> Portfolio Setup Form</h4>
                                </div>
                                <div class="card-body" style="overflow: hidden !important;">
                                    <p class="card-description text-white"></p>
                                    <form class="forms-sample" action="portfolio-store.php" method="post" enctype="multipart/form-data">
                                        <?php
                                        echo " 
                                           <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                                                                                                                         
                                       ";?>


                                      <div class="form-group row">
                                        <label for="project_type" class="col-sm-3 col-form-label text-white">Project Type</label>
                                        <div class="col-sm-9">
                                            <select type="text" class="form-control" id="project_type" name="project_type">
                                                <option class=" text-dark" value="PHP" selected>PHP</option>
                                                <option class=" text-dark" value="Laravel">Laravel</option>
                                                <option class=" text-dark" value="WordPress">WordPress</option>
                                            </select>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <label for="project_title" class="col-sm-3 col-form-label text-white">Project Title</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="project_title" name="project_title" placeholder="Project Title">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="link" class="col-sm-3 col-form-label text-white">Project Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="link" id="link" placeholder="http://">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="image" class="col-sm-3 col-form-label text-white">Image</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" name="image" id="image">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-common mr-3">Submit</button>
                                        <!--<button class="btn btn-light">Cancel</button>-->


                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

<?php include '../include/footer.php';?>