<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/PortfolioAdmin/PortfolioAdmin.php";

use App\PortfolioAdmin\PortfolioAdmin;
use App\Utility\Utility;


$fileName = $_FILES['image']['name'];

$source = $_FILES['image']['tmp_name'];

$destination = "PortfolioUploads/".$fileName;

move_uploaded_file($source,$destination);

$objProfilePictureAdmin =  new PortfolioAdmin();

$_POST['image'] = $fileName;

$objProfilePictureAdmin->setData($_POST);

$objProfilePictureAdmin->update();

Utility::redirect('view-portfolio.php');