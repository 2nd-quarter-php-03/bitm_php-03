<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once "../../../src/PortfolioAdmin/PortfolioAdmin.php";
require_once ("../../../src/Message/Message.php");

use App\PortfolioAdmin\PortfolioAdmin;

$viewSinglePortfolio = new PortfolioAdmin();
$singleData = $viewSinglePortfolio->view($_GET['id']);
echo json_encode($singleData);
//$msg = Message::message();