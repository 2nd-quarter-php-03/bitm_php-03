<?php
require_once "../../../vendor/autoload.php";
require_once "../../../src/PortfolioAdmin/PortfolioAdmin.php";
use App\Message\Message;
use App\PortfolioAdmin\PortfolioAdmin;
use App\Utility\Utility;


$fileName = time().$_FILES['image']['name'];

$source = $_FILES['image']['tmp_name'];
$destination = "PortfolioUploads/".$fileName;
move_uploaded_file($source,$destination);


$objProfilePictureAdmin =  new App\PortfolioAdmin\PortfolioAdmin();

$_POST['image'] = $fileName;

$objProfilePictureAdmin->setData($_POST);

$objProfilePictureAdmin->store();

Utility::redirect('add-portfolio.php');