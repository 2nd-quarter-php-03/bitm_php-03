<?php

$path = $_SERVER ['HTTP_REFERER'];
require_once "../../../vendor/autoload.php";
require_once "../../../src/PortfolioAdmin/PortfolioAdmin.php";

use App\PortfolioAdmin\PortfolioAdmin;
use App\Utility\Utility;

$object = new PortfolioAdmin();
$object->setData($_GET);
$object->delete();
Utility::redirect($path);