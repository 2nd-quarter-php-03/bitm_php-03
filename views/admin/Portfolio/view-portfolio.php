<?php
session_start();
require_once ("../../../vendor/autoload.php");
require_once "../../../src/PortfolioAdmin/PortfolioAdmin.php";
require_once ("../../../src/Message/Message.php");

use App\Message\Message;
use App\Utility\Utility;

use App\PortfolioAdmin\PortfolioAdmin;
$msg = Message::message();
$object = new PortfolioAdmin();

$allData = $object->index();

?>

<?php include '../include/head.php'; ?>
<?php include '../include/sidebar.php'; ?>




<div class="page-container">
    <div class="main-content">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="card bg-dark">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="card bg-dark">
                            <div class="card-header border-bottom" style="text-align: center">
                                <h4 class="card-title text-white" style="text-align: center;" > View Portfolio Form</h4>
                            </div>
                            <div class="card-body">
                                <p class="card-description text-white"></p>

                                <form class="forms-sample" action="portfolio-store.php" method="post" style="text-align: center">
                                    <?php
                                    echo " 
                                <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                             ";?>
                                    <div class="form-group row" style="text-align: center">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10">
                                            <table style="box-shadow: 0 0  25px #fdfff4;alignment: center;text-align: center">

                                                <thead class="table-bordered table-hover" style="text-align: center;alignment: center">
                                                <tr>
                                                    <th style="font-weight: bolder;font-size: small;text-align: center;color: #ffffff;padding: 20px">Serial</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff;padding: 20px">Project Type</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff;padding: 20px">Project Title</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center; color: #ffffff;padding: 20px">Project Link</th>
                                                    <th style="font-weight: bolder;font-size: medium;text-align: center;color: #ffffff;padding: 20px">Image</th>
                                                    <th style="text-align: center;font-weight: bolder;font-size: large;color: #ffffff;padding: 20px">Action</th>
                                                </tr>
                                                </thead>
                                                <?php
                                                $serial =1;
                                                foreach ($allData as $record)
                                                {
                                                    echo "                      
                                                <tbody class='table-bordered table-striped'>
                                                   <tr>
                                                        
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$serial</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->project_type</td>
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->project_title</td>                                                     
                                                        <td style='text-align:center;font-size: small;color: #ffffff'>$record->link</td>
                                                        <td style='text-align:center;color: #ffffff'><img style='padding-top: 10px' src='PortfolioUploads/$record->image' height='70' width='50' alt=''></td>
                                                         <td>
                                                            <button type='button' class='btn btn-info btn-xs' data-toggle='modal' data-target='#ordine' onClick='viewone($record->id)' data-keyboard='false' data-backdrop='static'>Edit<i class='fa fa-pencil'></i>
                                                             </button>
                                                       <a type='button' href='deletePortfolio.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-danger btn-xs'>Delete<i class='fa fa-trash-o'></i></a>
                                                     </td>
                                                    </tr>
                                                </tbody>
                                                 
                                                    ";
                                                    $serial++;
                                                }
                                                ?>
                                            </table>

                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include '../include/footer.php';?>


<script type="text/javascript">

    function confirm_delete(){

        return confirm("Are You Sure?");

    }
    function viewone(upid)
    {
        $.ajax({
            type: 'get',
            url: 'editPortfolio.php?id='+upid,
            success: function(data)
            {
                var obj = JSON.parse(data);
                $('#ordine input[name="id"]').val(obj.id);
                $('#ordine input[name="project_type"]').val(obj.project_type);
                $('#ordine input[name="project_title"]').val(obj.project_title);
                $('#ordine input[name="link"]').val(obj.link);

                $('#ordine #pimg').attr('src', 'PortfolioUploads/'+obj.image);



            }
        });
    }

    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )


    // <!--search Option-->


    function filtab() {
        var input = document.getElementById("sfdt");
        var filter = input.value.toUpperCase();
        var table = document.getElementById("dtc");
        var r=1, f=0;
        while(row=table.rows[r++])
        {
            var c=0, k=0;
            while(cell=row.cells[c++])
            {
                if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    k++;
                }
            }
            if(k>0){
                row.style.display = "";
                f=1;
            } else {
                row.style.display = "none";
            }
        }

    }

    function remimg() {
        $(".splt").remove();
    }

</script>


<div id="ordine" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content bg-dark">
            <div class="modal-header">
                <h4 class="modal-title">Update Profile table</h4>
                <button type="button" class="close" data-dismiss="modal" onclick="remimg()">×</button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">

                        <form class="forms-sample" action="portfolioUpdate.php" method="post" enctype="multipart/form-data">

                            <?php
                            echo " 
                                    <div id='message' style='color: #f8fffd;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                               ";?>

                            <input type="hidden" name="id" value="" />


                            <div class="form-group row">
                                <label for="project_type" class="col-sm-3 col-form-label text-white">Project Type</label>
                                <div class="col-sm-9">
                                    <select type="text" class="form-control" id="project_type" name="project_type">
                                        <option value="PHP" selected>PHP</option>
                                        <option value="Laravel">Laravel</option>
                                        <option value="WordPress">WordPress</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="project_title" class="col-sm-3 col-form-label text-white">Project Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="project_title" name="project_title" placeholder="Project Title">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="link" class="col-sm-3 col-form-label text-white">Project Link</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="link" id="link" placeholder="http://">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="image" class="col-sm-3 col-form-label text-white">Image</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control" name="image" id="image">
                                    <img id="pimg" height="80px" width="auto">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-common mr-3">Update</button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

