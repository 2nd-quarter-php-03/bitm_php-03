<?php
    $gurl = $_SERVER['REQUEST_URI'];
    $pieces = explode("/", $gurl);
    if(end($pieces) == "admin") {
        header('location: views/admin/');
    } else {
        header('location: views/front-end/');
    }
